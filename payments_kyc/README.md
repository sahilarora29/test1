This is a template project for Java based services which are deployed to AWS Elastic Beanstalk using Phaser.

# PROJECT_NAME

PROJECT_DESCRIPTION

## Minimum Prerequisites

* Java 8 SDK

## Build

To build the project run the following command:

```
> gradlew.bat build
```

## Run

```
> gradlew.bat bootRun
```

The server will start on port 8080, navigate to [http://localhost:8080/swagger-ui.html]
(http://localhost:8080/swagger-ui.html) to review the API documentation.  

## IDE

To load the project in eclipse run the following command:

```
> gradlew.bat eclipse
```

For IntelliJ use

```
> gradlew.bat idea
```

This project use lombok, please install it into your IDE. For details
https://projectlombok.org/download.html

## saas-spring-boot

This template includes the saas-spring-boot dependency, which implements a number of common features
and best practices. For details, see:
http://dbygitmsprod.pbi.global.pvt/saas-boot/saas-spring-boot

### Logging

By default, this project will use a developer friendly log for local development and a JSON-based
log file format in an AWS environment. See the included logback.xml for more information.