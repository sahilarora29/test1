#!/bin/bash

exitVal=0

phaserVersion=v1.5.0

rm -rf phaser
curl --header "PRIVATE-TOKEN:${GITLAB_PERSONAL_ACCESS_TOKEN}" http://dbygitmsprod.pbi.global.pvt/ansible/phaser/raw/master/install.sh | sh -s ${phaserVersion}
cd phaser

echo ${VAULT_PASS} > ./vault-password.txt
# alternate way: export the password value as VAULT_PASS variable, and then add a "-f ./pwdTool" argument to
#   the ./deploy call below

CHK=""
[[ ${CHECK_MODE} == 'true' ]] && CHK="--check"

SKIP_VER=""
[[ ${DONT_CHANGE_VERSION} == 'true' ]] && SKIP_VER="-s" && VERSION="0"

SKIP_OPTS=""
[[ ${DONT_CHANGE_OPTIONS} == 'true' ]] && SKIP_OPTS="-o"

PYTHONUNBUFFERED=1 \
ANSIBLE_HOST_KEY_CHECKING=false \
ANSIBLE_FORCE_COLOR=true \
./deploy -r ${REGION} -e ${ENV_TYPE} ${SKIP_VER} ${SKIP_OPTS} -v ${VERSION} master.yml ${CHK}
exitVal=$?

rm -f ./vault-password.txt
exit $exitVal
