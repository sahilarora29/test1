#!/bin/sh

phaserVersion="v1.5.4"
bold="\033[1m"
normal="\033[0m"
token=$GITLAB_ACCESS_TOKEN
user=$GITLAB_USER
phaserUrl='http://dbygitmsprod.pbi.global.pvt/ansible/phaser.git'
requirementFile='files/requirements.yml'

if [ x$1 != x ]
then
  echo "${bold}Using Phaser version $1${normal}"
  phaserVersion=$1
else
  echo "${bold}Using default Phaser version ${phaserVersion}${normal}"
fi

if [ x$2 != x ]
then
  if [[ "$2" == "ssh" ]]; then
    phaserUrl=git+ssh://git@dbygitmsprod.pbi.global.pvt/ansible/phaser.git
    requirementFile='files/requirements.ssh.yml'
    echo "${bold}Using gitlab authentication as $2${normal}"
  elif [[ "$2" == "token" ]]; then
    if [ "x$token" != "x" ] && [ "x$user" != "x" ]; then
      phaserUrl=http://${GITLAB_USER}:${GITLAB_ACCESS_TOKEN}@dbygitmsprod.pbi.global.pvt/ansible/phaser.git
      requirementFile='files/requirements.token.yml'
      cat 'files/requirements.token.yml.tmpl' | sed "s/GITLAB_USER/${GITLAB_USER}/g;s/GITLAB_ACCESS_TOKEN/${GITLAB_ACCESS_TOKEN}/g;w requirements.token.yml"
      echo "${bold}Using gitlab authentication as $2${normal}"
    else
      echo "${bold}For token authentication, export GITLAB_ACCESS_TOKEN and GITLAB_USER${normal}"
      exit 0
    fi
  fi
else
  echo "${bold}Using default gitlab authentication using .netrc file${normal}"
fi
# prepare requirements file for phaser download
cat << EOF > .phaser.requirements.yml
- src: ${phaserUrl}
  scm: git
  version: ${phaserVersion}
EOF

# install latest phaser build
ansible-galaxy install -r .phaser.requirements.yml -p .
rm .phaser.requirements.yml

# install phaser requirements
cd phaser
if [ -f ../requirements.token.yml ]; then
  mv ../requirements.token.yml files/requirements.token.yml
fi
ansible-galaxy install -r ${requirementFile}
cd ..
