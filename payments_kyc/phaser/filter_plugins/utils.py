#!/usr/bin/python
# -*- coding: utf-8 -*-

import yaml

try:
    import boto3
    import boto.iam
except ImportError:
    print "Python boto or boto3 module not installed"

from ansible.module_utils.basic import *
from ansible import errors
from distutils.version import StrictVersion

def to_json_file(filepath, dest=None):
    if dest is None:
        if not os.path.exists("tmp"):
            os.mkdir("tmp")
        dest = "tmp/out.json"

    yaml_file = _read_yaml(filepath)

    dest_f = open(dest, "w")
    json.dump(yaml_file, dest_f, indent=1)

    return dest_f.name

def _read_yaml(filepath):
    f = open(filepath, 'r')
    if f:
        return yaml.load(f) or dict()
    else:
        raise Exception("Could not find file: " + f)

def merge_dicts(d1, d2):
    d = d1.copy()

    for key, value in d2.items():
        if isinstance(value, dict):
            d[key] = merge_dicts(d.setdefault(key, {}), value)
        elif isinstance(value, list):
            d[key] = d[key] + value
        else:
            d[key] = value

    return d

def add_env_vars(optionSettings, envVars):
    for envVar in envVars:
        for k in envVar:
            beanstalkEnvVar = {'Namespace':'aws:elasticbeanstalk:application:environment'}
            beanstalkEnvVar['OptionName'] = k
            beanstalkEnvVar['Value'] = str(envVar[k])

            optionSettings.append(beanstalkEnvVar)

    return optionSettings

def env_var(default, var_name):
    return os.getenv(var_name, default)

def get_account_id(region):
    conn = boto.iam.connect_to_region(region)
    user = conn.get_user()

    arn = user.arn
    return arn.split("::",1)[1].split(":")[0]

def get_lambda_arn(name, region):
    aws_lambda = boto3.client('lambda', region_name=region)
    try:
        function = aws_lambda.get_function(FunctionName=name)
        return function['Configuration']['FunctionArn']
    except:
        e = sys.exc_info()[0]
        raise errors.AnsibleFilterError("Something wrong with lambda function: %s. Exception: %s" % name, e)

def get_role_arn(name, region):
    aws_iam = boto3.resource('iam', region_name=region)
    try:
        aws_role = aws_iam.Role(name)
        return aws_role.arn
    except:
        e = sys.exc_info()[0]
        raise errors.AnsibleFilterError("Something wrong with role: %s. Exception: %s" % name, e)

# Generate hosts list - in form of delimited string (eg. "node1.es.int,node2.es.int"), based on:
#   - base domain zone ("zone.int")
#   - node prefix ("node")
#   - amount of records to be generated
#   - (optional) port= number if needed
#   - (optional) separator= (default value: ",")
# EXAMPLE: {{ "zone.int" | gen_csv_str("instance", 3, 9300) }}
# RESULT:  "instance1.zone.int:9300,instance2.zone.int:9300,instance3.zone.int:9300"
def generate_hosts_csv(domain, node, counter, port=None, separator=","):
    for index in range(1, int(counter)+1):
        hosts = ("" if index == 1 else (hosts + separator)) + node + str(index) + "." + domain + ("" if port is None else (":" + str(port)))
    return hosts if 'hosts' in locals() else ""

class FilterModule(object):
    def filters(self):
        return {
            "get_account_id": get_account_id,
            "to_json_file": to_json_file,
            "merge_dicts": merge_dicts,
            "add_env_vars": add_env_vars,
            "env_var": env_var,
            "generate_hosts_csv": generate_hosts_csv,
            "get_lambda_arn": get_lambda_arn,
            "get_role_arn": get_role_arn
        }
