# Phaser

#### Report issues/concerns on the hipchat room here: [![HipChat Room](https://img.shields.io/badge/discuss-on%20hipchat-brightgreen.svg)](https://hipchat.pitneycloud.com/chat/room/324)

### New

Dynamo DB support:
```
serviceStorage:
  dynamodb:
    tableNamePrefix: (optional, defaults to "{{serviceInfo.product}}-{{regions[region]['regionIata']}}-{{vpcType}}-"
    tables:
      - name: table_name, required
        hash_key_name: (hash key column name, optional, defaults to id)
        hash_key_type: (hash key column type, optional, defaults to STRING, valid: STRING,NUMBER,BINARY)
        range_key_name: (range key column name, optional, defaults to create_date)
        range_key_type: (range key column type, optional, defaults to NUMBER)
        read_capacity: (read capacity, optional, defaults to 1)
        write_capacity: (write capacity, optional, defaults to 1)
        indexes: (indexes, optional, defaults not empty)
          - name: (index name, required)
            type: (index type, required, valid: all,global_all,global_include,global_keys_only,include,keys_only)
            hash_key_name: (hash key column name, required)
            range_key_name: (range key column name, optional)
            includes: (optional columns to include, defaults is empty)
              - (optional column to include)
            read_capacity: (read capacity, optional, defaults to 1)
            write_capacity: (write capacity, optional, defaults to 1)
```

This project deploys a service to AWS based on it's deploy.yml file.

## Ansible

Ansible playbooks for deploying services. Before running the playbooks make sure to install all the
necessary dependencies, see roles.txt.

```
> ansible-galaxy install -r files/requirements.yml
```

Next, export your AWS credentials if they are not stored in `~/.boto` config file

```
> export AWS_ACCESS_KEY_ID='XXXXX'
> export AWS_SECRET_ACCESS_KEY='YYYYY'
```

Now you can run the plays using the following command:

```
> ./deploy master.yml
```

## deploy.yml

Services are defined in a deploy.yml file. Currently Phaser supports deploying services using AWS Beanstalk and optionally RDS. An example of the `deploy.yml` for a service is shown below. Please refer to the project wiki for more [documentation](http://dbygitmsprod.pbi.global.pvt/ansible/phaser/wikis/home)

```
---

serviceInfo:
  product: embark
  name: server
  costCenter: 1007051
  techContact: harpreet.singh@pb.com
  artifact:
    groupId: com.pb.saas.embark
    id: embark-server
    classifier: beanstalk
    extension: zip
  dependencies: []

serviceStorage:
  rds:
    # Custom serviceStorage pre-deployment tasks
    preDeployTasks: myPreDeployStorage.yml
    # Custom serviceStorage post-deployment tasks
    postDeployTasks: myPostDeployStorage.yml
    # Enable/disable utf8 for mysql RDS
    utf8Enabled: 'true'
    # List of parameters
    parameters:
      innodb_large_prefix: '1'
    instance:
      dbEngineVersion: "5.6.23"
      dbInstanceSizeInGB: 15
      dbInstanceType: db.t2.medium
      dbName: embark
      dbPassword: "{{ crypt.dbs.embark.dbPassword }}"
      dbType: mysql
      dbUsername: embark
      # enable/disable replica
      # (single replica in the same availability zone, Aurora replication not supported)
      replicaEnabled: 'true'

serviceCompute:
  beanstalk:
    instanceType: t2.medium
    minInstances: 1
    maxInstances: 4
    minInstancesInService: 1
    commandTimeout: 480
    logPublicationControl: "false"
    # Beanstalk SNS notifications endpoint (to disable - leave blank or remove completely)
    snsNotificationsEndpoint: "dummy@email.com"
    healthURL: "/health"
    solutionStackName: "64bit Amazon Linux 2015.03 v1.4.3 running Docker 1.6.2"
    sslCertArn: "{{ ssl_cert.cert_arn }}"
    # Add following to use TCP Pass through from elb to instance, useful to offload
    # ssl in the instance instead of elb
    tcpPassThrough: 'y'
    # Add following to override default RootVolumeSize (number, in GiB)
    rootVolumeSize: 16
    telemetry:
      appDynamics:
        tier: API
        nodeNamePrefix: server
    envVars:
      - SPRING_DATASOURCE_URL: "{{ 'jdbc:mysql://%s:%s/%s' | format(serviceStorage.rds.instance.host,serviceStorage.rds.instance.port,serviceStorage.rds.instance.dbName) }}"
      - SPRING_DATASOURCE_USERNAME: "{{ serviceStorage.rds.instance.dbUsername }}"
      - SPRING_DATASOURCE_PASSWORD: "{{ serviceStorage.rds.instance.dbPassword }}"
      - SPRING_PROFILES_ACTIVE: "{{ serviceEnv.springProfile }}"
  lambda:
    #preTasks: tasks/lambda-pre-tasks.yml
    #postTask: tasks/lambda-post-tasks.yml
    # schedule based function
    functions:
      - name: lambdaFunctionName1
        schedule: "cron(30 15 * * ? *)"
        timeout: 180
        runtime: java8
        handler: com.pb.saas.lambdaFunctionName::lambdaFunction1
        ruleInput:
          input: "filedownloadprocess"
          bucket: "{{ some-bucket-name }}
        rolePolicies:
          - resource: "*"
            action:
              - "lambda:*"
              - "logs:*"
            effect: "Allow"
      # non-scheduled event based function
      - name: lambdaFuncitonName2
        timeout: 180
        runtime: java8
        handler: com.pb.saas.lambdaFunctionName::lambdaFunction2
        rolePolicies:
          - resource: "*"
            action:
              - "lambda:*"
              - "logs:*"
            effect: "Allow"

```

Environment specific details can be stored in `.deploy-config/{{ vpcType }}/main.yml` file. A `crypt.yml` file can also be used to store secrets for the service. An example of environment specific configuration is shown below.

```
---
serviceEnv:
  r53zone: hsingh.pitneycloud.com
  registerBeanstalkCnames: True
  serverSimpleUrl: https://embark-api.hsingh.pitneycloud.com
  uiSimpleUrl: https://embark.hsingh.pitneycloud.com
  springProfile: default
  members:
    Harpreet:
      pubKey: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9BkGF4dE5Ws/jJVrUXIW9Dg0pCI6WQVvWeesKo9Avby9z5zT+t1MyAmZC5zSyvEIs9ECROq5ZSR7+s0vnJPUOxprW85k0mKo76UiePXxbygAYF4h0SHhOnaqFRMJsZWee8uXbufK1SteQp0cnyuxicN8aac2eoy6QR1s5TDylRDHWJGtkxNkOPQd7Tzt+KGZjw+SIbkmz+i9Vy9IX3ifZIeyTANnaqhbEl22BY91IWC2bKxyhwQmG7hhkkNVqmjnkebv9gt1SgXsABDo6wGszpiinBR/02obUd6y5VEBmPNU2Pe3pI05DidVH+qBSypocE1rG+ubwtxvFfCtvz9qIqnLRA9Sloiplx5JV0b+i1jvCck+uh6mHb7esjHIG92C7suPcLrmSFbRXZbXPPsvVbAZKLrGkkCDZh2Qvrcse5tqmdj2OrVWprvsMdComUXWh3izHwBtV6AOcJV6T0eqhuYxt7kunKHvrpCI3mKpdcyMMzVaZDFG+p+pngtKuTu3hI/isAL2BBuxDtRsP0qG5lEGQJ+3sS45geLUsaht8ZtiVeqTqyXVynh1LLCqP7b1gGq6SL6pOUhbn5zPA1qSE83GPYCjyGYLvVt3MtOYIKsRejQSQaeIOY9Y96EgcXbvZ0TCz7vc3amS8APZUwFxBUwQjxUoMYJs/4ZneimQiAw== hs@hsingh.com
      email: 'harpreet.singh@pb.com'
    Jenkins:
      pubKey: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDrd3VKq/jCvsBmfVl9O0TEcamY+xGOjpIKdf0P/LtdarDI4Fziqa/QWubMY3tdaZtFZCyCEAnqVv0iqqcifuleczTVacpJRFcDglfDIDOSDycu9lEEXVebCJwHGlTXgPgLSAd7JvuTDQDurbg3ctYpjLDreLoCwKgMZ1jIaWnL6Raz6/Cf6cuueWwz8wcQbYebC0x2EGCIZyZNbiYzWd3O1RLBQLvhmVW54b+kAabVFu8pou8pXRFX7i154PHDcL0vqvFl6QXo+JzCAu5Xsb7bvAMSMI5Jx2k0Gpo4iW4aSHX+E8tZt5SXGxVAvaZMkBPga9SXt4iNZC6mgkYq1j01 jenkins@hub-build1

serviceStorage:
  rds:
    eventSubscribers:
      - protocol: email
        endpoint: "{{ serviceEnv.members.Harpreet.email }}"
    eventCategories: "availability,backup,configuration change,creation,deletion,failover,failure,low storage,maintenance,notification,read replica,recovery,restoration"
    eventSubscriptionEnabled: "false"
```

In order to deploy a service into AWS the VPC and other infrastructure for the account needs to dump facts to an S3 bucket so they can be referenced when the service is deployed. To dump and upload the facts reference the `tasks/dump_facts.yml` file in `cf-templates`.
