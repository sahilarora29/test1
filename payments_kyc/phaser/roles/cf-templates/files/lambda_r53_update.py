from __future__ import print_function
import json, logging, boto3, sys, re

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# ch = logging.StreamHandler(sys.stdout)
# logger.addHandler(ch)

def lambda_handler(event, context):
    autoscaling = boto3.client('autoscaling')
    ec2 = boto3.client('ec2')
    route53 = boto3.client('route53')

    logger.info('Event data: {}'.format(json.dumps(event)))
    message = json.loads(event['Records'][0]['Sns']['Message'])

    asg_name = message['AutoScalingGroupName']
    asg_event = message['Event']

    if asg_event in ['autoscaling:EC2_INSTANCE_LAUNCH', 'autoscaling:EC2_INSTANCE_TERMINATE']:
        logger.info('Processing ASG tags')
        as_response = autoscaling.describe_tags(
            Filters=[
                {
                    'Name': 'auto-scaling-group',
                    'Values': [ asg_name ]
                },
                {
                    'Name': 'key',
                    'Values': [ 'DomainMeta' ]
                }
            ],
            MaxRecords=1)

        logger.info(as_response)

        if len(as_response['Tags']) is 0:
            logger.error('[ERROR] ASG: {} does not define Route53 DomainMeta tag'.format(asg_name))
            sys.exit(1)
        else:
            tokens = as_response['Tags'][0]['Value'].split(':')
            route53tags = { 'HostedZoneId': tokens[0], 'RecordName': tokens[1] }
        logger.info('Found tags: {}'.format(json.dumps(route53tags)))
        logger.info('Retrieving Instances in ASG')

        as_response = autoscaling.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name], MaxRecords=1)

        instanceIds = []
        for instance in as_response['AutoScalingGroups'][0]['Instances']:
            instanceIds.append(instance['InstanceId'])

        ec2_response = ec2.describe_instances(InstanceIds=instanceIds)

        resourceRecords = []
        for reservation in ec2_response['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Code'] in [0, 16]:
                    resourceRecords.append({'Value': instance['NetworkInterfaces'][0]['PrivateIpAddress']})

        resourceRecords = sorted(resourceRecords, reverse=True)
        logger.info('Found following resources: {}'.format(resourceRecords))

        logger.info('Updating Route53 record')
        r53_response = route53.change_resource_record_sets(
            HostedZoneId=route53tags['HostedZoneId'],
            ChangeBatch={
                'Changes': [
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': route53tags['RecordName'],
                            'Type': 'A',
                            'TTL': 10,
                            'ResourceRecords': resourceRecords
                        }
                    }
                ]
            })

        logger.info('Route53 request response: {}'.format(r53_response))
