#!/usr/bin/python
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

DOCUMENTATION = """
---
module: dynamodb
short_description: Create, update or delete AWS Dynamo DB tables.
description:
  - Create or delete AWS Dynamo DB tables.
  - Can update the provisioned throughput on existing tables.
  - Returns the status of the specified table.
  - Add stream to table
author: Alan Loi (@loia), Harpreet Singh
requirements:
  - "boto3 >= 1.2.1"
options:
  state:
    description:
      - Create or delete the table
    required: false
    choices: ['present', 'absent']
    default: 'present'
  name:
    description:
      - Name of the table.
    required: true
  hash_key_name:
    description:
      - Name of the hash key.
      - Required when C(state=present).
    required: false
    default: null
  hash_key_type:
    description:
      - Type of the hash key.
    required: false
    choices: ['STRING', 'NUMBER', 'BINARY']
    default: 'STRING'
  range_key_name:
    description:
      - Name of the range key.
    required: false
    default: null
  range_key_type:
    description:
      - Type of the range key.
    required: false
    choices: ['STRING', 'NUMBER', 'BINARY']
    default: 'STRING'
  read_capacity:
    description:
      - Read throughput capacity (units) to provision.
    required: false
    default: 1
  write_capacity:
    description:
      - Write throughput capacity (units) to provision.
    required: false
    default: 1
extends_documentation_fragment:
    - aws
    - ec2
"""

EXAMPLES = '''
# Create dynamo table with hash and range primary key
- dynamodb_table:
    name: my-table
    region: us-east-1
    hash_key_name: id
    hash_key_type: STRING
    range_key_name: create_time
    range_key_type: NUMBER
    read_capacity: 2
    write_capacity: 2
# Update capacity on existing dynamo table
- dynamodb_table:
    name: my-table
    region: us-east-1
    read_capacity: 10
    write_capacity: 10
# Delete dynamo table
- dynamodb_table:
    name: my-table
    region: us-east-1
    state: absent
'''

RETURN = '''
table_status:
    description: The current status of the table.
    returned: success
    type: string
    sample: ACTIVE
'''

try:
    import boto
    import boto3
    # import boto.dynamodb2
    # from boto.dynamodb2.table import Table
    # from boto.dynamodb2.fields import HashKey, RangeKey
    # from boto.dynamodb2.types import STRING, NUMBER, BINARY
    from botocore.exceptions import ClientError
    from boto.exception import BotoServerError, NoAuthHandlerFound, JSONResponseError
    HAS_BOTO = True

except ImportError:
    HAS_BOTO = False


DYNAMO_TYPE_MAP = {
    'STRING': 'S',
    'NUMBER': 'N',
    'BINARY': 'B'
}


def create_or_update_dynamo_table(client, module):
    table_name = module.params.get('name')
    hash_key_name = module.params.get('hash_key_name')
    hash_key_type = module.params.get('hash_key_type')
    range_key_name = module.params.get('range_key_name')
    range_key_type = module.params.get('range_key_type')
    read_capacity = module.params.get('read_capacity')
    write_capacity = module.params.get('write_capacity')
    stream_enabled = module.params.get('stream_enabled')
    stream_view_type = module.params.get('stream_view_type')

    if range_key_name:
        attrs = [
            {'AttributeName': hash_key_name, 'AttributeType': DYNAMO_TYPE_MAP.get(hash_key_type)},
            {'AttributeName': range_key_name, 'AttributeType': DYNAMO_TYPE_MAP.get(range_key_type)}
        ]
        schema = [
            {'AttributeName': hash_key_name, 'KeyType': 'HASH'},
            {'AttributeName': range_key_name, 'KeyType': 'RANGE'}
        ]
    else:
        attrs = [
            {'AttributeName': hash_key_name, 'AttributeType': DYNAMO_TYPE_MAP.get(hash_key_type)}
        ]
        schema = [
            {'AttributeName': hash_key_name, 'KeyType': 'HASH'}
        ]

    throughput = {
        'ReadCapacityUnits': read_capacity,
        'WriteCapacityUnits': write_capacity
    }

    if stream_enabled:
        stream = {
            'StreamEnabled': stream_enabled,
            'StreamViewType': stream_view_type
        }
    else:
        stream = {'StreamEnabled': stream_enabled}

    result = dict(
        region=module.params.get('region'),
        table_name=table_name,
        hash_key_name=hash_key_name,
        hash_key_type=hash_key_type,
        range_key_name=range_key_name,
        range_key_type=range_key_type,
        read_capacity=read_capacity,
        write_capacity=write_capacity,
        stream_enabled=stream_enabled,
        stream_view_type=stream_view_type
    )

    try:
        table = boto3.resource('dynamodb').Table(table_name)

        if dynamo_table_exists(client, table):
            result['changed'] = update_dynamo_table(table, throughput=throughput, stream=stream, check_mode=module.check_mode)
        else:
            if not module.check_mode:
                client.create_table(AttributeDefinitions=attrs,
                                    TableName=table_name,
                                    KeySchema=schema,
                                    ProvisionedThroughput=throughput,
                                    StreamSpecification=stream)
            result['changed'] = True

        if not module.check_mode:
            retries=0

            while table.table_status != 'ACTIVE' or retries == 4:
                time.sleep(5 * (2**retries))
                retries += 1
                table.reload()

            result['table_status'] = table.table_status
            result['table_arn'] = table.table_arn
            if stream_enabled:
                result['stream_arn'] = table.latest_stream_arn
                result['stream_label'] = table.latest_stream_label

    except BotoServerError:
        result['msg'] = 'Failed to create/update dynamo table due to error: ' + traceback.format_exc()
        module.fail_json(**result)
    else:
        module.exit_json(**result)

def delete_dynamo_table(client, module):
    table_name = module.params.get('name')

    result = dict(
        region=module.params.get('region'),
        table_name=table_name,
    )

    try:
        table = boto3.resource('dynamodb').Table(table_name)

        if dynamo_table_exists(client, table):
            if not module.check_mode:
                table.delete()
            result['changed'] = True
        else:
            result['changed'] = False

    except BotoServerError:
        result['msg'] = 'Failed to delete dynamo table due to error: ' + traceback.format_exc()
        module.fail_json(**result)
    else:
        module.exit_json(**result)


def dynamo_table_exists(client, table):
    try:
        client.describe_table(TableName=table.name)
        return True

    except ClientError, e:
        if e.message and 'ResourceNotFoundException' in e.message:
            return False
        else:
            raise e


def update_dynamo_table(table, throughput=None, stream=None, check_mode=False):
    if has_throughput_changed(table, throughput):
        if not check_mode:
            table.update(ProvisionedThroughput=throughput)

        return True

    if has_stream_changed(table, stream):
        if not check_mode:
            table.update(StreamSpecification=stream)

        return True

    return False

def has_throughput_changed(table, new_throughput):
    if not new_throughput:
        return False

    return new_throughput['ReadCapacityUnits'] != table.provisioned_throughput['ReadCapacityUnits'] or \
           new_throughput['WriteCapacityUnits'] != table.provisioned_throughput['WriteCapacityUnits']

def has_stream_changed(table, new_stream):
    return (new_stream['StreamEnabled'] and not table.stream_specification) or \
           (not new_stream['StreamEnabled'] and table.stream_specification) or \
           (new_stream['StreamEnabled'] and new_stream['StreamViewType'] != table.stream_specification['StreamViewType'])

def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        state=dict(default='present', choices=['present', 'absent']),
        name=dict(required=True, type='str'),
        hash_key_name=dict(required=True, type='str'),
        hash_key_type=dict(default='STRING', type='str', choices=['STRING', 'NUMBER', 'BINARY']),
        range_key_name=dict(type='str'),
        range_key_type=dict(default='STRING', type='str', choices=['STRING', 'NUMBER', 'BINARY']),
        read_capacity=dict(default=1, type='int'),
        write_capacity=dict(default=1, type='int'),
        stream_enabled=dict(default=False, type='bool'),
        stream_view_type=dict(type='str', choices=['NEW_IMAGE','OLD_IMAGE','NEW_AND_OLD_IMAGES','KEYS_ONLY']),
    ))

    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True)

    if not HAS_BOTO:
        module.fail_json(msg='boto required for this module')

    region, ec2_url, aws_connect_params = get_aws_connection_info(module)
    if not region:
        module.fail_json(msg='region must be specified')

    boto3.setup_default_session(region_name=region)
    client = boto3.client('dynamodb')

    state = module.params.get('state')
    if state == 'present':
        create_or_update_dynamo_table(client, module)
    elif state == 'absent':
        delete_dynamo_table(client, module)


# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

if __name__ == '__main__':
    main()
