#!/usr/bin/python

DOCUMENTATION = '''
---
module: sns_subscriptions
short_description: updates sns subscriptions
description:
    - updates the sns subscription for provided topic.
options:
  topicArn:
    description:
      - The topic ARN for which to subscribe
    required: true
  region:
    description:
      - The aws region (e.g. 'us-east-1')
    required: true
  subscriptions:
    descriptions:
      - A list of subscriptions. Subscription structure must be {'protocol':'<protocol>','endpoint':'<endpoint>'}. example: {'protocol':'email','endpoint':'nilkantha.aryal@pb.com'}
    author: Nilkantha Aryal
'''


EXAMPLES = '''
# Sync subscriptions:

- sns_subscriptions:
    topicArn: "arn:aws:sns:us-west-1:833608163426:horizon-dev-rds-horizon-defaultSnsTopicArn-10BTV9D50ID54"
    region: "us-west-1"
    subscriptions:
      - protocol: email
        endpoint: nilkantha.aryal@pb.com

Note: The subscriptions with 'pending confirmation' status cannot be removed.
Warning: If the list of subscriptions is empty, all the subscriptions are removed.

'''

import sys
import time

try:
    import boto.sns
    import boto.logs
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def get_topic_name(topic_arn):
    temp = topic_arn.split(':')
    return temp[len(temp)-1]

def describe_subscriptions(sns_conn, topic):
    subscriptions = sns_conn.get_all_subscriptions_by_topic(topic)
    subscriptions = subscriptions['ListSubscriptionsByTopicResponse']['ListSubscriptionsByTopicResult']['Subscriptions']
    return subscriptions

def get_pending_subscriptions(subscriptions):
    pending_subscriptions=[]
    for subscription in subscriptions:
        subs = dict()
        if subscription['SubscriptionArn'] == "PendingConfirmation":
            subs['protocol'] = subscription['Protocol']
            subs['endpoint'] = subscription['Endpoint']
            pending_subscriptions.append(subs)
    return pending_subscriptions

def get_removed_subscriptions(sns_conn, subscriptions, endpoints, check_mode):
    removed_subscriptions = []
    for subscription in subscriptions:
        subs = dict()
        pending_subscription = subscription['SubscriptionArn'] == "PendingConfirmation"
        endpoint = subscription['Endpoint']
        if endpoint not in endpoints and not pending_subscription:
            subs['protocol'] = subscription['Protocol']
            subs['endpoint'] = subscription['Endpoint']
            removed_subscriptions.append(subs)
            if not check_mode:
                sns_conn.unsubscribe(subscription['SubscriptionArn'])
    return removed_subscriptions

def get_unchanged_subscriptions(subscriptions, endpoints):
    unchanged_subscriptions = []
    for subscription in subscriptions:
        subs = dict()
        pending_subscription = subscription['SubscriptionArn'] == "PendingConfirmation"
        endpoint = subscription['Endpoint']
        if endpoint in endpoints or pending_subscription:
            subs['protocol'] = subscription['Protocol']
            subs['endpoint'] = subscription['Endpoint']
            if pending_subscription:
                subs['reason'] = "Subscription pending confirmation"
            unchanged_subscriptions.append(subs)
    return unchanged_subscriptions

def add_subscriptions(sns_conn, subscriptions, topic, check_mode):
    if not check_mode:
        for subscription in subscriptions:
            sns_conn.subscribe(topic, subscription['protocol'], subscription['endpoint'])

def get_new_endpoints(subscriptions):
    endpoints = []
    for subscription in subscriptions:
        endpoints.append(subscription['endpoint'])
    return endpoints

def get_subscriptions_to_add(subscriptions, subscriptions_not_changed):
    subs = []
    for subscription in subscriptions:
        endpoint = subscription['endpoint']
        sub_exists = False
        for subs_unchanged in subscriptions_not_changed:
            if endpoint == subs_unchanged['endpoint']:
                sub_exists = True
        if not sub_exists:
            subs.append(subscription)
    return subs

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            subscriptions   =   dict(type='list', required=True),
            topicArn        =   dict(required=True),
            region          =   dict(required=True)
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    topic = module.params['topicArn']
    subscriptions = module.params['subscriptions']
    region = module.params['region']

    sns_conn = boto.sns.connect_to_region(region)

    if subscriptions is None:
        module.exit_json(msg="Subscriptions is None, please pass empty list if you need to remove all subscriptions")

    existing_subscriptions = describe_subscriptions(sns_conn, topic)

    new_endpoints = get_new_endpoints(subscriptions)

    subscriptions_removed = get_removed_subscriptions(sns_conn, existing_subscriptions, new_endpoints, module.check_mode)
    subscriptions_not_changed = get_unchanged_subscriptions(existing_subscriptions, new_endpoints)
    subscriptions_added = get_subscriptions_to_add(subscriptions, subscriptions_not_changed)
    add_subscriptions(sns_conn,subscriptions_added, topic, module.check_mode)
    subscriptions_pending = get_pending_subscriptions(existing_subscriptions)

    result = {}
    if len(subscriptions_removed) > 0 or len(subscriptions_added) > 0:
        result = dict(changed=True, output=dict(removed=subscriptions_removed, unchanged=subscriptions_not_changed, added=subscriptions_added))
    else:
        result = dict(changed=False, output=dict(unchanged=subscriptions_not_changed))
    if len(subscriptions_pending) > 0:
        result['output']['pending_confirmation'] = subscriptions_pending

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
