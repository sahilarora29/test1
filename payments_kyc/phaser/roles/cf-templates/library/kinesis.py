#!/usr/bin/python

DOCUMENTATION = '''
---
module: kinesis
short_description: manage kinesis streams
description:
    - Creates and delets Kinesis streams
options:
  name:
    description:
      - Name of the stream
    required: true
  shard_count:
    description:
      - The number of shards that the stream uses. Required if state is present
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates a stream
- kinesis:
    name: test_stream
    shard_count: 1
    region: "{{ region }}"

# Remove a stream
- kinesis:
    name: test_stream
    state: absent
    region: "{{ region }}"
'''

import sys
import time

try:
    import boto3
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def delete_stream(kinesis, existing_stream, name, check_mode):
    message = ""
    changed = False
    stream = {'StreamARN': ''}

    if existing_stream:
        message = "Stream will be deleted"

        if not check_mode:
            kinesis.delete_stream(StreamName=name)
            message = "Stream deleted"

        stream = existing_stream['StreamDescription']
        changed = True
    else:
        message = "Stream doesn't exist"
        changed = False

    return message, changed, stream

def create_stream(kinesis, existing_stream, name, check_mode, shard_count, retention_period):
    message = ""
    changed = False
    stream = {'StreamARN': ''}

    if existing_stream:
        return update_stream(kinesis, existing_stream, name, check_mode, shard_count, retention_period)
    else:
        message = "Stream will be created"
        if not check_mode:
            kinesis.create_stream(StreamName=name,ShardCount=shard_count)
            stream = wait_for_status(kinesis, name, 'ACTIVE')
            if retention_period > 24:
                kinesis.increase_stream_retention_period(StreamName=name,
                RetentionPeriodHours=retention_period)
                stream = wait_for_status(kinesis, name, 'ACTIVE')

            message = "Stream created"
        changed = True

    return message, changed, stream

def update_stream(kinesis, existing_stream, name, check_mode, shard_count, retention_period):
    message = ""
    changed = False
    stream = existing_stream['StreamDescription']

    if retention_period >= 24 and \
        existing_stream['StreamDescription']['RetentionPeriodHours'] != retention_period:
        message = "Stream retention period will be changed"

        if not check_mode:
            if existing_stream['StreamDescription']['RetentionPeriodHours'] > retention_period:
                kinesis.decrease_stream_retention_period(StreamName=name, RetentionPeriodHours=retention_period)
            elif existing_stream['StreamDescription']['RetentionPeriodHours'] < retention_period:
                kinesis.increase_stream_retention_period(StreamName=name, RetentionPeriodHours=retention_period)

            stream = wait_for_status(kinesis, name, 'ACTIVE')
            message = "Stream retention period changed"

        changed = True
    else:
        message = "Stream already exists"
        changed = False

    return message, changed, stream

def wait_for_status(kinesis, name, status):
    stream = kinesis.describe_stream(StreamName=name)
    retries = 0

    while stream['StreamDescription']['StreamStatus'] != status or retries == 4:
        time.sleep(5 * (2**retries))
        retries += 1
        stream = kinesis.describe_stream(StreamName=name)

    return stream['StreamDescription']

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state      = dict(default='present', choices=['present', 'absent']),
            name       = dict(default=None, required=True, type='str'),
            shard_count= dict(default=1, type='int'),
            retention_period = dict(default=24, type='int')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state      = module.params['state']
    name       = module.params['name']
    shard_count= module.params['shard_count']
    retention_period = module.params['retention_period']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    kinesis = boto3.client('kinesis')

    try:
        stream = kinesis.describe_stream(StreamName=name)
    except:
        print sys.exc_info()[0]
        stream = None

    result = {}
    if state == 'present':
        message, changed, stream = create_stream(kinesis, stream, name, module.check_mode, shard_count, retention_period)
    elif state == 'absent':
        message, changed, stream = delete_stream(kinesis, stream, name, module.check_mode)

    result = dict(changed=changed,output=message,stream=stream)

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
