#!/usr/bin/python

DOCUMENTATION = '''
---
module: lambda
short_description: manage lambda functions
description:
    - Creates and deletes Lambda functions
options:
  name:
    description:
      - Name of the function
    required: true
  description:
    description:
      - Description of the function
    required: false
  runtime:
    description:
      - The runtime environment for the Lambda function you are uploading.
    required: false
    choices: ["nodejs", "java8", "python2.7"]
    default: "nodejs"
  handler:
    description:
      - The function within your code that Lambda calls to begin execution.
        For Node.js, it is the module-name .*export* value in your function.
        For Java, it can be package.class-name::handler or package.class-name
    required: false
  role:
    description:
      - The Amazon Resource Name (ARN) of the IAM role that Lambda assumes when
        it executes your function to access any other Amazon Web Services (AWS)
        resources
    required: false
  timeout:
    description:
      - The function execution time at which Lambda should terminate the function.
        Because the execution time has cost implications, we recommend you set
        this value based on your expected execution time. The default is 3 seconds.
    required: false
    default: 3
  memory:
    description:
      - The amount of memory, in MB, your Lambda function is given. Lambda uses
        this memory size to infer the amount of CPU and memory allocated to your
        function. Your function use-case determines your CPU and memory
        requirements. For example, a database operation might need less memory
        compared to an image processing function. The default value is 128 MB.
        The value must be a multiple of 64 MB
    required: false
    default: 128
  publish:
    description:
      - This boolean parameter can be used to request AWS Lambda to create the
        Lambda function and publish a version as an atomic operation.
    requied: false
    default: 'true'
    choices: [ "true", "false" ]
  code:
    description:
      - dictionary with S3Bucket and S3Key where code can be found
    required: false
  sha256:
    description:
      - sha256 sum of code ZIP file
    required: false
  schedule:
    description:
      - Cloud watch events scheduling expression. For example, "cron(0 20 * * ? *)", "rate(5 minutes)".
    required: false
  state:
    description:
      - Whether to create or delete the function.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION
        or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates a lambda function
- lambda:
    name: test_function
    handler: index.handler
    role: "{{ lambda.role.arn }}"
    code:
        S3Bucket: foo
        S3Key: bar.zip
    region: "{{ region }}"

# Remove lambda function
- lambda_source:
    name: test_function
    region: "{{ region }}"
    state: absent
'''

import sys
import time
import json

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def cc(key):
    return "".join([token.capitalize() for token in key.split('_')])

def compare_configuration(lambdaFunc, rule, ruleInputJson, targets, schedule, params):
    updates = []
    config = lambdaFunc['Configuration']

    if 'VpcConfig' in config:
        vpc_config = config['VpcConfig']

        for param in ['subnet_ids', 'security_group_ids']:
            ccparam = cc(param)
            if sorted(vpc_config[ccparam]) != sorted(params[param]):
                updates.append(('{}'.format(ccparam), vpc_config[ccparam], params[param]))

    if config['CodeSha256'] != params['sha256'] and params['sha256'] is not None:
        updates.append(('CodeSha256', config['CodeSha256'], params['sha256']))
    if config['Role'] != params['role']:
        updates.append(('Role', config['Role'], params['role']))
    if config['Handler'] != params['handler']:
        updates.append(('Handler', config['Handler'], params['handler']))
    if config['Timeout'] != params['timeout']:
        updates.append(('Timeout', config['Timeout'], params['timeout']))
    if config['MemorySize'] != params['memory']:
        updates.append(('MemorySize', config['MemorySize'], params['memory']))

    if rule and schedule is not None and rule['ScheduleExpression'] != schedule:
        updates.append(('ScheduleExpression', rule['ScheduleExpression'], schedule))
    if not rule and schedule is not None:
        updates.append(('ScheduleExpression', 'new', schedule))

    if targets:
        for target in targets:
            if 'Arn' in target and target['Arn'] == config['FunctionArn']:
                if 'Input' in target and target['Input'] != ruleInputJson:
                    updates.append(('ruleInput', target['Input'], ruleInputJson))

    if not targets and ruleInputJson:
        updates.append(('ruleInput', 'new', ruleInputJson))

    return updates

def create_rule(module, aws_events, aws_lambda, funcName, ruleName, ruleInputJson, schedule, functionArn):
    rule = {}
    try:
        rule = aws_events.put_rule(Name=ruleName,
                                   ScheduleExpression=schedule,
                                   State='ENABLED',
                                   Description='Trigger for lambda function {}'.format(funcName))
    except:
        module.fail_json(changed=False, msg="Exception in put_rule: {}".format(sys.exc_info()))

    try:
        aws_events.put_targets(Rule=ruleName,
                               Targets=[{
                                 'Id': 'lambda',
                                 'Arn': functionArn,
                                 'Input': ruleInputJson
                               }])
    except:
        module.fail_json(changed=False, msg="Exception in put_targets: {}".format(sys.exc_info()))

    try:
        aws_lambda.add_permission(FunctionName=funcName,
                                  StatementId='trigger-via-' + ruleName,
                                  Action='lambda:InvokeFunction',
                                  Principal='events.amazonaws.com',
                                  SourceArn=rule['RuleArn'])
    except ClientError, e:
        if not e.message or 'ResourceConflictException' not in e.message:
            raise e
    except:
        module.fail_json(changed=False, msg="Exception in add_permission: {}".format(sys.exc_info()))

    return rule

def remove_rule(aws_events, aws_lambda, funcName, ruleName):
    if len(ruleName) > 64:
        return

    try:
        aws_events.remove_targets(Rule=ruleName,
                                  Ids=[ 'lambda' ])
        aws_lambda.remove_permission(FunctionName=funcName,
                                     StatementId='trigger-via-' + ruleName)
        aws_events.delete_rule(Name=ruleName)
    except ClientError, e:
        if not e.message or 'ResourceNotFoundException' not in e.message:
            raise e


def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state               = dict(default='present', choices=['present', 'absent']),
            name                = dict(type='str', required=True),
            description         = dict(type='str', default=''),
            runtime             = dict(default='nodejs', type='str', choices=['nodejs', 'java8', 'python2.7']),
            handler             = dict(type='str', required=False),
            role                = dict(type='str', required=False),
            timeout             = dict(type='int', default=3),
            memory              = dict(type='int', default=128),
            publish             = dict(type='bool', default=True),
            code                = dict(type='dict', required=False),
            sha256              = dict(type='str', required=False),
            subnet_ids          = dict(type='list', required=False, default=[]),
            security_group_ids  = dict(type='list', required=False, default=[]),
            schedule            = dict(type='str', required=False),
            ruleName            = dict(type='str', required=False),
            ruleInput           = dict(type='dict', required=False)
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state               = module.params['state']
    name                = module.params['name']
    runtime             = module.params['runtime']
    handler             = module.params['handler']
    role                = module.params['role']
    timeout             = module.params['timeout']
    memory              = module.params['memory']
    publish             = module.params['publish']
    code                = module.params['code']
    sha256              = module.params['sha256']
    description         = module.params['description']
    schedule            = module.params['schedule']
    subnet_ids          = module.params['subnet_ids']
    security_group_ids  = module.params['security_group_ids']
    ruleName            = module.params['ruleName']
    ruleInput           = module.params['ruleInput']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    aws_lambda = boto3.client('lambda')
    aws_events = boto3.client('events')

    rule = None
    if not ruleName:
        ruleName = name + '-trigger'

    # Additional check for empty schedule
    # as default() filter will pass an empty value
    # even if you use None as default value
    if schedule == '':
        schedule = None

    try:
        lambdaFunc = aws_lambda.get_function(FunctionName=name)
    except:
        lambdaFunc = None

    try:
        rule = aws_events.describe_rule(Name=ruleName) if schedule else None
    except:
        rule = None

    try:
        targets = aws_events.list_targets_by_rule(Rule=ruleName)['Targets'] if rule else None
    except:
        targets = None

    ruleInputJson = json.dumps(ruleInput) if ruleInput else ""

    result = {}
    if lambdaFunc:
        if state == 'absent':
            if not module.check_mode:
                remove_rule(aws_events, aws_lambda, name, ruleName)
                aws_lambda.delete_function(FunctionName=name)
            result = dict(changed=True, output="Function {} has been deleted".format(name))
        else:
            updates = compare_configuration(lambdaFunc, rule, ruleInputJson, targets, schedule, module.params)
            if len(updates) > 0:
                if not module.check_mode:
                    # we need explicitly populate vpcConfig with empty values
                    # if either subnets or security groups lists are empty or not defined
                    vpcConfig = dict(SubnetIds=[], SecurityGroupIds=[]) if (subnet_ids == [] or security_group_ids == []) else dict(SubnetIds=subnet_ids, SecurityGroupIds=security_group_ids)

                    lambdaUpdateFunc = aws_lambda.update_function_configuration(FunctionName=name,
                                                                              Role=role,
                                                                              Handler=handler,
                                                                              Description=description,
                                                                              Timeout=timeout,
                                                                              MemorySize=memory,
                                                                              VpcConfig=vpcConfig)

                    scheduleChange = next((i for i, item in enumerate(updates) if item[0] in ['ScheduleExpression','ruleInput']), None)
                    if scheduleChange is not None:
                        create_rule(module, aws_events, aws_lambda, name, ruleName, ruleInputJson, schedule, lambdaUpdateFunc['FunctionArn'])
                        #module.fail_json(changed=False, msg="rule: {}".format(rule))

                    # as code check requires separate function
                    # we need to check for code change
                    codeChange = next((i for i, item in enumerate(updates) if item[0] == 'CodeSha256'), None)
                    if codeChange is not None:
                        lambdaCodeChange = aws_lambda.update_function_code(FunctionName=name,
                                                                           S3Bucket=code['S3Bucket'],
                                                                           S3Key=code['S3Key'])


                result = dict(changed=True, output=updates)
            else:
                result = dict(changed=False, output="Function already exists")

            if not module.check_mode and schedule is None:
                remove_rule(aws_events, aws_lambda, name, ruleName)
    else:
        if state == 'present':
            if not module.check_mode:
                vpcConfig = dict(SubnetIds=[], SecurityGroupIds=[]) if subnet_ids == [] else dict(SubnetIds=subnet_ids, SecurityGroupIds=security_group_ids)
                lambdaCreateFunc = aws_lambda.create_function(FunctionName=name,
                                                        Runtime=runtime,
                                                        Role=role,
                                                        Handler=handler,
                                                        Code=code,
                                                        Description=description,
                                                        Timeout=timeout,
                                                        MemorySize=memory,
                                                        Publish=publish,
                                                        VpcConfig=vpcConfig)
                if schedule is not None:
                    create_rule(module, aws_events, aws_lambda, name, ruleName, ruleInputJson, schedule, lambdaCreateFunc['FunctionArn'])

                result = dict(changed=True, output=lambdaCreateFunc)
            else:
                result = dict(changed=False, output="Function does not exist")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
