#!/usr/bin/python

DOCUMENTATION = '''
---
module: s3_lambda_event
short_description: Adds, updates or deletes lambda s3 event notifications
description:
    - Adds, updates or deletes lambda s3 event notifications.
options:
  bucket:
    description:
      - Name of the bucket
    required: true
  configuration_id:
    description:
      - Name of the bucket
    required: true
  lambda_function_arn:
    description:
      - Name of the bucket
    required: true
  events:
    description:
      - Name of the bucket
    required: true
  prefix:
    description:
      - Filter prefix
  suffix:
    description:
      - Filter suffix
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new lambda permission
- s3_lambda_event:
    bucket:
    lambda_function_arn:
    configuration_id:
    prefix:
    suffix:

# Remove lambda permission
- s3_lambda_event:
    bucket:
    configuration_id:
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def check_updates(old, new):
    updates = []

    if 'LambdaFunctionArn' in old and 'LambdaFunctionArn' in new:
        if old['LambdaFunctionArn'] != new['LambdaFunctionArn']:
            updates.append(('LambdaFunctionArn', old['LambdaFunctionArn'], new['LambdaFunctionArn']))
    elif 'LambdaFunctionArn' not in old and 'LambdaFunctionArn' in new:
        updates.append(('LambdaFunctionArn', 'None', new['LambdaFunctionArn']))

    if 'Id' in old and 'Id' in new:
        if new['Id'] != old['Id']:
            updates.append(('Id', old['Id'], new['Id']))
    elif 'Id' not in old and 'Id' in new:
        updates.append(('Id', 'None', new['Id']))

    if 'Events' in old and 'Events' in new:
        if sorted(new['Events']) != sorted(old['Events']):
            updates.append(('Events', old['Events'], new['Events']))
    elif 'Events' not in old and 'Events' in new:
        updates.append(('Events', 'None', new['Events']))

    try:
        old_filter_rules = old['Filter']['Key']['FilterRules']
    except KeyError:
        old_filter_rules = []

    try:
        new_filter_rules = new['Filter']['Key']['FilterRules']
    except KeyError:
        new_filter_rules = []

    if sorted(old_filter_rules) != sorted(new_filter_rules):
        updates.append(('FilterRules', old_filter_rules, new_filter_rules))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state               = dict(default='present', choices=['present', 'absent']),
            bucket              = dict(required=True, type='str'),
            lambda_function_arn = dict(required=True, type='str'),
            configuration_id    = dict(type='str'),
            events              = dict(type='list'),
            prefix              = dict(type='str'),
            suffix              = dict(type='str')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state               = module.params['state']
    bucket              = module.params['bucket']
    lambda_function_arn = module.params['lambda_function_arn']
    configuration_id    = module.params['configuration_id']
    events              = module.params['events']
    prefix              = module.params['prefix']
    suffix              = module.params['suffix']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    aws_s3 = boto3.client('s3')

     # check if event notifications exist
    try:
        facts = aws_s3.get_bucket_notification_configuration(Bucket=bucket)
        facts.pop('ResponseMetadata')
    except (ClientError, ParamValidationError, MissingParametersError) as e:
        module.fail_json(msg='Error retrieving s3 event notification configuration: {0}'.format(e))

    current_state = 'absent'
    existing_configuration = {}
    current_lambda_configs = []
    result = {}

    if 'LambdaFunctionConfigurations' in facts:
        current_lambda_configs = facts.pop('LambdaFunctionConfigurations')
        for config in current_lambda_configs:
            if config['Id'] == configuration_id:
                existing_configuration = config
                current_lambda_configs.remove(config)
                current_state = 'present'
                break

    if state == 'present':
        # build configurations
        new_configuration = dict(Id=configuration_id)
        new_configuration.update(LambdaFunctionArn=lambda_function_arn)
        new_configuration.update(Events=events)

        filter_rules = []
        if prefix: filter_rules.append(dict(Name='Prefix', Value=str(prefix)))
        if suffix: filter_rules.append(dict(Name='Suffix', Value=str(suffix)))
        if filter_rules: new_configuration.update(Filter=dict(Key=dict(FilterRules=filter_rules)))

        # check for changes
        updates = check_updates(existing_configuration, new_configuration)

        if len(updates) > 0:
            current_lambda_configs.append(new_configuration)
            facts.update(LambdaFunctionConfigurations=current_lambda_configs)

            try:
                if not module.check_mode:
                    aws_s3.put_bucket_notification_configuration(Bucket=bucket,
                                                                 NotificationConfiguration=facts)
                result = dict(changed=True, output="S3 event notification provisioned", updates=updates)
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error creating S3 event notification for lambda: {0}'.format(e))
        else:
            result = dict(changed=False, output="S3 event notification {} already present".format(configuration_id))

    else:
        if current_state == 'present':
            if current_lambda_configs:
                facts.update(LambdaFunctionConfigurations=current_lambda_configs)
            try:
                if not module.check_mode:
                    aws_s3.put_bucket_notification_configuration(Bucket=bucket,
                                                                 NotificationConfiguration=facts)
                result = dict(changed=True, output="S3 event notification removed")
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                module.fail_json(msg='Error creating S3 event notification for lambda: {0}'.format(e))

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
