#!/usr/bin/python

DOCUMENTATION = '''
---
module: wait_for_s3_object
short_description: Waits for S3 object to appear
description:
    - Waits for S3 object to appear
options:
  bucket:
    description:
      - Name of the bucket
    required: true
  key:
    description:
      - Object path including name
    required: true
  timeout:
    description:
      - Timeout (in seconds). Default: 120
    required: true
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new lambda permission
- wait_for_s3_object:
    bucket:
    key:
    timeout:
'''

# Ouptut example:
# {
#     changed: False,
#     ouptut: "Describing message",
#     exists: True|False
# }

import sys
import time

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            bucket              = dict(required=True, type='str'),
            key                 = dict(required=True, type='str'),
            timeout             = dict(default=120, type='int')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    bucket  = module.params['bucket']
    key     = module.params['key']
    timeout = module.params['timeout']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    aws_s3 = boto3.client('s3')

    curtime = int(time.time())
    while time.time() < curtime + timeout:
        try:
            aws_s3.head_object(Bucket=bucket, Key=key)
        except ClientError as e:
            if e.response['Error']['Code'] == "404":
                exists = False
                time.sleep(10)
            else:
                module.fail_json(msg='Error when trying to get S3 object details: {0}'.format(e))
        else:
            exists = True
            break

    if exists:
        output = "Found object s3://{0}/{1} in {2} seconds".format(bucket, key, int(time.time()-curtime))
    else:
        output = "Unable to find object s3://{0}/{1} in {2} seconds".format(bucket, key, int(time.time()-curtime))

    result = dict(changed=False, output=output, exists=exists)
    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
