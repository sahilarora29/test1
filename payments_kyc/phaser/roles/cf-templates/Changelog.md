## Changelog

#### v2.6.0 - February 19 2017
- Switch all modules to pb_* format (from ansible-modules repository, see Readme.md for details/instructions)
- Add templates and tasks for ASG-based ECS cluster
- Add templates and tasks for ASG-based Consul cluster
- Add lambda code for Route53 record update in Consul cluster
- Add support for DNS-related options in both VPC and VPCv2 templates/tasks

#### v2.5.0 - January 17 2017
- Add support for VPC with NAT gateway service instead of NAT instances
- Add support for ap-northeast-2, ap-south-1, eu-west-2, eu-central-1, ca-central-1 & us-east-2 regions
- Add ansible module for managing kinesis streams, `kinesis.py`
- Add support for ansible 2.X
- Add `wait_for_s3` module
- Add templates and tasks to create SQS queues and Elasticache instances
- Add templates and tasks to create VPN
- Add scripts for creating sumo logic collectors
- Add ansible module for managing lambda functions, `lambda.py` & `lambda_source.py`
- Add support for default and custom RDS parameter groups
- Add support for Aurora RDS instances
- Add ansible module for managing dynamodb tables and streams, `dynamodb.py`
- Add support for sandbox, performance and pre-prod environments

#### v2.1.7 - July 30 2015
- Add support for check mode in `iam_cert` module
- Add support for highly available NAT instances
- Add SNS subscription for notifications from RDS instances

#### v2.1.6 - July 22 2015
- Register NAT hosts with ansible after creation
- Add `iam_cert.py` module for uploading certs

#### v2.1.5 - July 17 2015
- Add support for HVM NAT AMIs

#### v2.1.4 - July 14 2015
- Add option to use reserved PB subnet CIDRs when creating VPC
- Add option to skip `ops` VPC when creating VPCs
- Add options to toggle SSH/DNS/Peering for VPCs

#### v2.1.3 - June 18 2015
- Add public IPs to Cloudformation output

Added more output to VPC template:
- public IPs of NAT instances

#### v2.1.2 - June 11 2015
- Fix issue with RDS cloudformation to prevent issues with dependencies

#### v2.1.1 - May 5 2015
- Add ability to specify maintenance windows and subscriptions for RDS

#### v2.1.0 - April 21 2015
- Reduce the size of the VPC to 2 subnets per AZ, one public and one private. The VPC will be provisioned across 2 AZs.

#### v2.0.0 - April 16 2015
- This is the initial version of this Ansible role. Previous versions in this repository were not structured for use with `ansible-galaxy`
