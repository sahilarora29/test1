# Creates a RDS-Aurora cluster in a VPC using cf-templates/rds/simple-rds-aurora.json
# CF template.
# Inputs:
#   - region: AWS Region
#   - costCenter: PB Cost Center
#   - techContact: email address for admin contact
#   - product: name of PB product which owns this resource
#   - vpcType: Type of VPC to this instance will be deployed in: choose from:
#             dev, qa, preview
#   - dbInstaceType:  AWS instance type, defaults to db.r3.large
#   - vpcId: AWS Id of the VPC, defaults to id of last vpc created by ansible
#   - vpcDefaultSg: AWS Security Group Id for the VPC's default security group
#   - appSubnetCidr1: Cidr of first subnet which is allowed to connect to the
#             instance, defaults to the first private subnet in the vpc
#   - appSubnetCidr2: Cidr of second subnet which is allowed to connect to the
#             instance, defaults to the second private subnet in the vpc
#   - dbPort: Port number on which to run the DB service, defaults to 3306
#   - dbName: The name of the initial database of this instance that was provided
#             at create time
#   - backupRetentionPeriod: The number of days for which automatic DB snapshots
#             are retained, default = 7
#   - dbEngine: The name of the database engine that the DB instance uses, default = mysql
#   - dbEngineVersion: The version number of the database engine to use, default = 5.6.19b
#   - dbUsername: name of the user to provision in the database
#   - dbPassword: password for the user, if not specified creates a random password
#             If a random password is created it saved to a file in the current dir
#   - dbSubnetId1: First subnet in the DB subnet group, defaults to third private
#             subnet provisioned in last vpc
#   - dbSubnetId2: Second subnet in the DB subnet group, defaults to fourth private
#             subnet provisioned in last vpc
#
# Outputs:
#   - rds: contains outputs from the CF template

- name: Generate random password?
  set_fact:
    dbPassword: "{{ lookup('password', '../../' + dbName + '-password.txt length=10 chars=ascii_letters,digits') }}"
  when: dbPassword is not defined

- name: Provisioning RDS-Aurora cluster
  pb_cfn:
    stack_name: "{{ product}}-{{ vpcType }}-rds-aurora-{{ dbName }}"
    state: "present"
    region: "{{ region }}"
    template: "{{ templateDir }}/rds/simple-rds-aurora.json"
    template_parameters:
      costCenter: "{{ costCenter }}"
      techContact: "{{ techContact }}"
      product: "{{ product }}"
      regionIata: "{{ regions[region]['regionIata'] }}"
      environment: "{{ vpcType }}"
      dbInstanceType: "{{ dbInstanceType | default('db.r3.large') }}"
      vpcId: "{{ vpcId | default(vpc.stack_outputs.vpcId )}}"
      vpcDefaultSg: "{{ vpcDefaultSg | default(vpc.stack_outputs.sgDefault) }}"
      appSubnetCidr1: "{{ connectedSubnetCidr | default(vpcCfg[vpcType].subnetPri1) }}"
      appSubnetCidr2: "{{ connectedSubnetCidr | default(vpcCfg[vpcType].subnetPri2) }}"
      dbPort: "{{ dbPort | default (3306) }}"
      dbName: "{{ dbName }}"
      StorageEncrypted: "{{ StorageEncrypted}}"
      KmsKeyId: "{{ KmsKeyId }} "
      backupRetentionPeriod: "{{ backupRetentionPeriod | default(1) }}"
      dbEngine: "{{ dbEngine | default('aurora') }}"
      dbEngineVersion: "{{ dbEngineVersion | default('5.6.10a') }}"
      dbUsername: "{{ dbUsername }}"
      dbPassword: "{{ dbPassword }}"
      dbSubnetId1: "{{ dbSubnetId1 | default(vpc.stack_outputs.subnetPri1) }}"
      dbSubnetId2: "{{ dbSubnetId2 | default(vpc.stack_outputs.subnetPri2) }}"
      preferredBackupWindow: "{{ preferredBackupWindow | default('09:30-10:00') }}"
      preferredMaintenanceWindow: "{{ preferredMaintenanceWindow | default('sat:09:00-sat:09:30') }}"
      rdsEventCategories: "{{ rdsEventCategories | default('') }}"
      rdsEventSubscriptionEnabled: "{{ rdsEventSubscriptionEnabled | default('false')}}"
  register: rds

- name: RDS-Aurora cluster provisioned
  debug:
    msg: "{{ rds }}"
  when: rds.changed
