# AWS Provisioning
[![HipChat Room](https://img.shields.io/badge/discuss-on%20hipchat-brightgreen.svg)](https://hipchat.pitneycloud.com/chat/room/324)

### New
Added option to add a read replica for mysql RDS. Currently only supported for single read replica in the same region.


This project is an [Ansible](http://ansible.com) role which includes several AWS Cloud Formation templates used by Pitney Bowes SaaS applications.

### Cloud Formation Template Inventory

1. VPC with subnets in two zones, 2 public subnet, 2 private subnets. Two NAT instances.
2. RDS
3. Elastic Beanstalk application using latest docker environment (DEPRECATED)
4. Create Ubuntu based bastion host (DEPRECATED)
5. Elastic cache
6. RDS Aurora
7. SQS
8. VPC with subnets in two zones and NAT gateway service
9. VPN
10. ECS cluter (based on CloudFormation and ASG)
11. Consul cluster (based on CloudfFormation, ASG and Lambda)

### Dependency changes introduced by v2.6.0 update

With 2.6.0 update all modules from `/library` were renamed (prefix `pb_` was added to all PBs custom modules) and moved to ansible-modules repository (http://dbygitmsprod.pbi.global.pvt/ansible/ansible-modules), and tasks were also updated to use new naming scheme.

From now on it is required to import `ansible-modules` together with `cf-templates`, and load `ansible-modules` as role instead. Examples:
`requirements.txt`:
```
# PB CF Templates
- src: git+http://dbygitmsprod.pbi.global.pvt/pbsaas/cf-templates.git
  path: roles
  version: v2.6.0

# PB Ansible Modules
- src: git+http://dbygitmsprod.pbi.global.pvt/ansible/ansible-modules.git
  path: roles
  version: master
```
`main.yml`:
```
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    vpcType: "{{ 'dev' | env_var('VPC_TYPE') }}"
  roles:
    - ansible-modules
  ...
```

### Ansible role

Ansible is an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks such as continuous deployments or zero downtime rolling updates. To include this role in your Ansible project you can install it using `ansible-galaxy`. For example to install version 2.0.0 of this role run the following command:

```
ansible-galaxy install git+http://dbygitmsprod.pbi.global.pvt/pbsaas/cf-templates.git,v2.0.0
```
