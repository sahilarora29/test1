#!/usr/bin/python

DOCUMENTATION = '''
---
module: sumologic_source
short_description: manage sumologic sources
description:
    - Creates, deletes and lists Sumologic Sources per Collector
options:
  name:
    description:
      - Source name
    required: true
  access_id:
    description:
      - Sumologic access ID
    required: true
  access_key:
    description:
      - Sumologic access key
    required: true
  collector_id:
    description:
      - Sumologic collector ID
    required: true
  source_type:
    description:
      - Source type (for now only HTTP is supported)
    required: true
    default: HTTP
    choices: [ "HTTP" ]
  message_per_request:
    description:
      - Enable one message Per Request
    type: boolean
    default: true
    required: false
  category:
    description:
      - Source category
    required: false
  filters:
    description:
      - Source filters
    required: false
    default: []
  automatic_date_parsing:
    required: false
    default: true
  multiline_processing_enabled:
    required: false
    default: True
  use_autoline_matching:
    required: false
    default: true
  force_time_zone:
    required: false
    default: false
  cutoff_timestamp:
    required: false
    type: int
    default: 0
  encoding:
    required: false
    default: 'UTF-8'
  state:
    description:
      - Whether to create, delete or list source.
    required: true
    default: present
    choices: [ "present", "absent", "list" ]
    aliases: []
'''

EXAMPLES = '''
# Creates a source
  - pb_sumologic_source:
      state: present
      name: source_name
      collector_id: colelctor_id
      access_id: access_id
      access_key: access_key
      source_type: "HTTP"
      message_per_request: false

# Remove a source
- pb_sumologic_source:
    name: source_name
    access_id: access_id
    access_key: access_id
    collector_id: collector_id
    state: absent
'''

import sys
import time
import json

try:
    from sumologic import SumoLogic
except ImportError:
    print "failed=True msg='sumologic-sdk python package required for this module'"
    sys.exit(1)

def compare_sources(new, existing):
    new_keys = set(new.keys())
    existing_keys = set(existing.keys())
    intersect_keys = new_keys.intersection(existing_keys)
    added = new_keys - existing_keys
    removed = existing_keys - new_keys
    modified = {o : (new[o], existing[o]) for o in intersect_keys if new[o] != existing[o]}
    same = set(o for o in intersect_keys if new[o] == existing[o])
    return added, removed, modified, same

def prepare_source(source):
    mod_source = dict(source)
    if 'id' in mod_source:
        del mod_source['id']
    if 'url' in mod_source:
        del mod_source['url']
    if 'alive' in mod_source:
        del mod_source['alive']
    return mod_source

def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state = dict(default='present', choices=['present', 'absent', 'list']),
            access_id = dict(type='str', required=True),
            access_key = dict(type='str', required=True),
            name = dict(type='str', required=True),
            collector_id = dict(type='str', required=True),
            source_type = dict(default='HTTP', choices=['HTTP']),
            message_per_request = dict(type='bool', default=True),
            category = dict(type='str'),
            filters = dict(type='list', default=[]),
            automatic_date_parsing = dict(type='bool', default=True),
            multiline_processing_enabled = dict(type='bool', default=True),
            use_autoline_matching = dict(type='bool', default=True),
            force_time_zone = dict(type='bool', default=False),
            cutoff_timestamp = dict(type='int', default=0),
            encoding = dict(type='str', default='UTF-8')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state                        = module.params['state']
    name                         = module.params['name']
    collector_id                 = module.params['collector_id']
    source_type                  = module.params['source_type']
    access_id                    = module.params['access_id']
    access_key                   = module.params['access_key']
    filters                      = module.params['filters']
    automatic_date_parsing       = module.params['automatic_date_parsing']
    multiline_processing_enabled = module.params['multiline_processing_enabled']
    use_autoline_matching        = module.params['use_autoline_matching']
    force_time_zone              = module.params['force_time_zone']
    cutoff_timestamp             = module.params['cutoff_timestamp']
    encoding                     = module.params['encoding']

    try:
        sumo = SumoLogic(access_id, access_key)
    except:
        module.fail_json(changed=False, msg="Unable to get sources")

    try:
        sources = sumo.sources(collector_id)
    except:
        sources = list()

    if state == 'list':
        module.exit_json(**dict(changed=False, output=sources))

    result = {}
    existing_source = dict()
    existing_etag = ""

    new_source = dict(
        name=name, 
        sourceType=source_type,
        filters=filters,
        automaticDateParsing=automatic_date_parsing,
        multilineProcessingEnabled=multiline_processing_enabled,
        useAutolineMatching=use_autoline_matching,
        forceTimeZone=force_time_zone,
        cutoffTimestamp=cutoff_timestamp,
        encoding=encoding)

    if 'source_type' in module.params and module.params['source_type'] == "HTTP":
        if 'message_per_request' in module.params:
           new_source.update(
               dict(messagePerRequest=module.params['message_per_request']))
        else:
           module.exit_json(**dict(changed=False, msg="message_per_request should be defined for HTTP source type"))

    if 'category' in module.params and module.params['category']:
        new_source.update(dict(category=module.params['category']))
   
    if len(sources) > 0:
        for source in sources:
            if source['name'] == name and source['sourceType'] == source_type:
                _src, existing_etag = sumo.source(collector_id, source['id'])
                existing_source = _src['source']
                break

    if existing_source:
        if state == 'present':
            _added, _removed, _modified, _same = compare_sources(new_source, prepare_source(existing_source))
            if _added or _removed or _modified:
                result = dict(
                    changed=True, msg="Source is going to be updated")
                if not module.check_mode:
                    new_source['id'] = existing_source['id']
                    r = sumo.update_source(collector_id, dict(source=new_source), existing_etag)
                    result = dict(changed=True, msg="Source was updated!",
                                  source=json.loads(r.text)['source'])
            else:
                result = dict(changed=False, msg="Source already exists", source=existing_source)
        elif state == 'absent':
            if not module.check_mode:
                try:
                    sumo.delete_source(collector_id, existing_source)
                    result = dict(changed=True, msg="Source has been deleted")
                except:
                    module.fail_json(changed=False, msg="Unable to delete source")
            else:
                result = dict(changed=True, msg="Source is going to be deleted")
    else:
        if state == 'present':
            if not module.check_mode:
                try:
                    # collector_verbose, etag = sumo.collector(collector_id)
                    # headers = { 'If-Match': etag.strip("\\\"") }
                    # r = sumo.post('/collectors/' + str(collector_id) + '/sources', dict(source=new_source))
                    r = sumo.create_source(
                        collector_id, dict(source=new_source))
                    result = dict(changed=True, msg="Source has been created",
                        source=json.loads(r.text)['source'])
                except:
                    module.fail_json(
                        changed=False, 
                        msg="Unable to create source: {}".format(sys.exc_info()))
            else:
                result = dict(
                    changed=True, msg="New source is going to be created")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
