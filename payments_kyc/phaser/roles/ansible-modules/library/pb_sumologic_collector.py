#!/usr/bin/python

DOCUMENTATION = '''
---
module: sumologic_collector
short_description: manage sumologic collectors
description:
    - Creates, deletes and lists Sumologic Collectors
options:
  name:
    description:
      - Sumologic collector name
    required: true
  access_id:
    description:
      - Sumoloigc account access_id
    required: true
  access_key:
    description:
      - Sumoloigc account access_key
    required: true
  type:
    description:
      - Type of collector (for now only Hosted type is supported)
    required: true
    default: hosted
    choices: [ "hosted" ]
  state:
    description:
      - Whether to create, delete or list collectors.
    required: true
    default: list
    choices: [ "list", "present", "absent" ]
    aliases: []
'''

EXAMPLES = '''
# Creates Sumologic collector
- sumologic_collector:
    name: "collector-name"
    access_id: access_id@domain.com
    access_key: password
    state: present
    type: hosted
  register: sumo_collector
'''

import sys
import time
import json
import requests
try:
    import cookielib
except ImportError:
    import http.cookiejar as cookielib

try:
    from sumologic import SumoLogic
except ImportError:
    print "failed=True msg='sumologic-sdk python package required for this module'"
    sys.exit(1)

def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state          = dict(default='list', choices=['present', 'absent', 'list']),
            name           = dict(type='str', required=True),
            access_id      = dict(type='str', required=True),
            access_key     = dict(type='str', required=True),
            type           = dict(default='hosted', choices=['hosted'])
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state = module.params['state']
    access_id = module.params['access_id']
    access_key = module.params['access_key']
    name = module.params['name']
    collector_type = module.params['type']

    try:
        sumo = SumoLogic(access_id, access_key)
    except:
        module.fail_json(changed=False, msg="Unable connect to SumoLogic")

    try:
        collectors = sumo.collectors()
    except:
        module.fail_json(changed=False, msg="Unable to get collectors")

    existing_collector = dict()
    existing_etag = ""

    for collector in collectors:
        if collector['name'] == name and collector['collectorType'] == collector_type.title():
            try:
                existing_collector, existing_etag = sumo.collector(collector['id'])
            except:
                module.fail_json(changed=False, msg="Unable to get exact collector")

    if state == 'list':
        if 'collector' in existing_collector:
            result = dict(
                changed=False,
                collector=existing_collector['collector'],
                etag=existing_etag.strip("\\\""))
        else:
            result = dict(changed=False, collector=None)
    elif state == 'present':
        if not existing_collector:
            try:
                _collector = {'collector': {'collectorType': collector_type.title(), 'name': name}}
                response = sumo.post(
                    '/collectors', _collector)
                result = dict(changed=True, collector=json.loads(
                    response.text)['collector'])
            except:
                module.fail_json(changed=False, msg="Error creating hosted collector")
        else:
            result = dict(changed=False,
                          msg="Collector already present!",
                          collector=existing_collector['collector'])
    elif state == 'absent':
        if not existing_collector:
            result = dict(changed=False, msg="Nothing to delete!")
        else:
            try:
                sumo.delete_collector(existing_collector)
                result = dict(changed=True, msg="Collector was successfully deleted!")
            except:
                module.fail_json(changed=False, msg="Error deleting collector")


    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
