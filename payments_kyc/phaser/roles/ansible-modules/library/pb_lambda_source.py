#!/usr/bin/python

DOCUMENTATION = '''
---
module: lambda_source
short_description: manage lambda event sources
description:
    - Creates and deletes Lambda Event Sources
options:
  source_arn:
    description:
      - ARN of the source
    required: true
  function:
    description:
      - Name of the function
  batch_size:
    description:
      - Largest number of records Lambda will retrieve from the event source.
    default: 100
  start_pos:
    description:
      - The position in the stream where lambda should start reading
    default: TRIM_HORIZON
    choices: ['TRIM_HORIZON', 'LATEST']
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates a event source
- lambda_source:
    source_arn: "{{ dynamodb.table_arn }}"
    function: mail-aggregation

# Remove a event source
- lambda_source:
    source_arn: "{{ dynamodb.table_arn }}"
    function: mail-aggregation
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def source_exists(sources, name):
    source = sources.EventSourceMappings
    return source.State == 'Enabled' or source.State == 'Creating' or \
     source.State == 'Enabling' or source.State == 'Updating'

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state      = dict(default='present', choices=['present', 'absent']),
            source_arn = dict(required=True, type='str'),
            function   = dict(required=True, type='str'),
            batch_size = dict(default=100, type='int'),
            start_pos  = dict(default='TRIM_HORIZON', type='str', choices=['TRIM_HORIZON','LATEST'])
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state      = module.params['state']
    source_arn = module.params['source_arn']
    function   = module.params['function']
    batch_size = module.params['batch_size']
    start_pos  = module.params['start_pos']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    aws_lambda = boto3.client('lambda')

    sources = aws_lambda.list_event_source_mappings(EventSourceArn=source_arn,
                                                    FunctionName=function)

    if len(sources['EventSourceMappings']) > 0:
        source = sources['EventSourceMappings'][0]
    else:
        source = None

    result = {}
    if source:
        if state == 'absent':
            if not module.check_mode:
                aws_lambda.delete_event_source_mapping(UUID=source['UUID'])
            result = dict(changed=True, output="Source deleted")
        else:
            result = dict(changed=False, output="Source already exists")
    else:
        if state == 'present':
            if not module.check_mode:
                aws_lambda.create_event_source_mapping(EventSourceArn=source_arn,
                                                       FunctionName=function,
                                                       Enabled=True,
                                                       BatchSize=batch_size,
                                                       StartingPosition=start_pos)
                result = dict(changed=True, output="Source created")
            else:
                result = dict(changed=False, output="Source doesn't exist")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
