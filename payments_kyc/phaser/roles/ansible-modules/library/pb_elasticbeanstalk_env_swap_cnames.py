#!/usr/bin/python

DOCUMENTATION = '''
---
module: pb_elasticbeanstalk_env_swap_cnames
short_description: swap CNAMEs between two beanstalk environments
options:
  app_name:
    description:
      - Application Name
    required: false
    default: null
  source_environment_name:
    description:
      - Name of the CNAME's source environment
    required: false
    default: null
  destination_environment_name:
    description:
      - Name of the destination environment for the CNAME
    required: false
    default: null
author: Romel Bumanlag
extends_documentation_fragment: aws
'''

EXAMPLES = '''
# Swap Environment CNAMEs
- pb_elasticbeanstalk_env_swap_cnames:
    app_name: applicationName
    source_environment_name: sourceEnvName
    destination_environment_name: destinationEnvName
'''
import string
import time
import random

try:
    import boto
    import boto.beanstalk.exception
    import boto.beanstalk
    HAS_BOTO = True
except ImportError:
    HAS_BOTO = False

def wait_for_health(ebs, app_name, env_name, health, wait_timeout):
    VALID_BEANSTALK_HEALTHS = ('Green', 'Yellow', 'Red', 'Grey')

    if not health in VALID_BEANSTALK_HEALTHS:
        raise ValueError(health + " is not a valid beanstalk health value")

    timeout_time = time.time() + wait_timeout

    while 1:
        #print "Waiting for beanstalk %s to turn %s" % (app_name, health)

        result = ebs.describe_environments(application_name=app_name, environment_names=[env_name])
        current_health = result["DescribeEnvironmentsResponse"]["DescribeEnvironmentsResult"]["Environments"][0]["Health"]
        #print "Current health is: %s" % current_health

        if current_health == health:
            #print "Beanstalk %s has turned %s" % (app_name, health)
            return result

        if time.time() > timeout_time:
            raise ValueError("The timeout has expired")

        time.sleep(15)
	
def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            app_name                     = dict(required=True),
            source_environment_name      = dict(required=True),
            destination_environment_name = dict(required=True),
            wait_timeout   = dict(default=300, type='int')
        ),
    )
    module = AnsibleModule(argument_spec=argument_spec)

    if not HAS_BOTO:
        module.fail_json(msg='boto required for this module')

    app_name = module.params.get('app_name')
    src_env_name = module.params.get('source_environment_name')
    dest_env_name = module.params.get('destination_environment_name')
    wait_timeout = module.params.get('wait_timeout')
    
    result = {}
    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)
    aws_connect_kwargs.pop("validate_certs")

    try:
        eb = boto.connect_beanstalk(**aws_connect_kwargs)

    except boto.exception.NoAuthHandlerFound, e:
        module.fail_json(msg='No Authentication Handler found: %s ' % str(e))
    except Exception, e:
        module.fail_json(msg='Failed to connect to Beanstalk: %s' % str(e))
    
    src_env = eb.describe_environments(application_name=app_name, environment_names=[src_env_name])
    dest_env = eb.describe_environments(application_name=app_name, environment_names=[dest_env_name])
	
    try:
        eb.swap_environment_cnames(
            source_environment_name=src_env_name,
            destination_environment_name=dest_env_name
        )
    except boto.exception.BotoServerError as error:
        module.fail_json(msg="Unable to swap CNAMEs: {0}".format(error))
    else:
        envs=wait_for_health(eb, app_name, dest_env_name, 'Green', wait_timeout)
        envs = envs["DescribeEnvironmentsResponse"]["DescribeEnvironmentsResult"]["Environments"]
        result = dict(changed=True, env=envs[0])
		
    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
