#!/usr/bin/python

DOCUMENTATION = '''
---
module: lambda_permission
short_description: manage lambda permissions
description:
    - Creates and deletes Lambda Permissions
options:
  function_name:
    description:
      - Name of the function
     required: true
  statement_id:
    description:
      - A unique statement identifier
    required: true
  action:
    description:
      - The AWS Lambda action you want to allow in this statement
    required: true
  principal:
    description:
      - The principal who is getting this permission
    required: true
  source_arn:
    description:
      - This is optional; however, when granting Amazon S3 permission to invoke your function, you should specify this field with the bucket Amazon Resource Name (ARN) as its value
  source_account:
    description:
      - The AWS account ID (without a hyphen) of the source owner
  qualifier:
    description:
      - You can use this optional query parameter to describe a qualified ARN using a function version or an alias name
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new lambda permission
- lambda_permission:
    function_name:
    statement_id:
    action:
    principal:
    state: present

# Remove lambda permission
- lambda_permission:
    function_name:
    statement_id:
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
    import botocore
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state              = dict(default='present', choices=['present', 'absent']),
            function_name      = dict(required=True, type='str'),
            statement_id       = dict(required=True, type='str'),
            action             = dict(default=None, type='str'),
            principal          = dict(default=None, type='str'),
            source_arn         = dict(default=None, type='str'),
            source_account     = dict(default=None, type='str'),
            qualifier          = dict(default=None, type='str')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state              = module.params['state']
    function_name      = module.params['function_name']
    statement_id       = module.params['statement_id']
    action             = module.params['action']
    principal          = module.params['principal']
    source_arn         = module.params['source_arn']
    source_account     = module.params['source_account']
    qualifier          = module.params['qualifier']
    region             = module.params['region']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    aws_lambda = boto3.client('lambda', region_name=region)

    result = {}
    if state == 'absent':
        if not module.check_mode:
            try:
                aws_lambda.remove_permission(FunctionName=function_name,
                                             StatementId=statement_id,
                                             Qualifier=qualifier)
            except:
                module.fail_json(changed=False, msg="#2 Exception in add_permission: {}".format(sys.exc_info()))
        result = dict(changed=True, output="Permission deleted")

    if state == 'present':
        if action is None or principal is None:
            #module.exit_json(dict(failed=True, msg=))
            module.fail_json(changed=False, msg="'Action' and 'Principal' are required for 'present' state")
        if not module.check_mode:
            try:
                params_add_payload = {
                    'FunctionName': function_name,
                    'StatementId': statement_id,
                    'Action': action,
                    'Principal': principal
                }
                if qualifier is not None: params_add_payload['Qualifier'] = qualifier
                if source_arn is not None: params_add_payload['SourceArn'] = source_arn
                if source_account is not None: params_add_payload['SourceAccount'] = source_account
                aws_lambda.add_permission(**params_add_payload)
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "ResourceConflictException":
                    module.exit_json(changed=False, output="Permission with StatementId: {} already exists".format(statement_id))
                else:
                    module.fail_json(changed=False, msg="Exception in add_permission: {}".format(sys.exc_info()))
            except:
                module.fail_json(changed=False, msg="Exception in add_permission: {}".format(sys.exc_info()))
        result = dict(changed=True, output="Permission created")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
