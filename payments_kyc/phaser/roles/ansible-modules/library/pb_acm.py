#!/usr/bin/python

DOCUMENTATION = '''
---
module: pb_acm
short_description: manage certificates in aws certificate manager
description:
    - Creates certificate requests, lists certificates
options:
  name:
    description:
      - name of the certificate
    required: true
  domain_name:
    description:
      - domain name for the certificate
    required: true
  state:
    description:
      - Whether to create certificate request, delete certificate
    default: present
    choices: [ "present", "absent"]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates or returns certificate arn
- pb_acm:
    name: 'star-example-com'
    domain_name: '*.example.com'
    region: "{{ region }}"

# deletes certificate
- pb_acm:
    name: 'star-example-com'
    domain_name: '*.example.com'
    state: absent
    region: "{{ region }}"
'''

import sys
import time

try:
    import boto3
    import boto
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def delete_cert(acm, existing_certs, cert_domain, check_mode):
    message = "certificate will be deleted"
    if not check_mode:
        acm.delete_certificate(CertificateArn=cert_arn)
        message = cert_domain + " certificate deleted"
    return message

def request_certificate(acm, cert_domain, alt_names, name, check_mode):
    message = "certificate request will be created"
    response = {}
    if not check_mode:
        if alt_names is None or len(alt_names) == 0:
            response = acm.request_certificate(DomainName=cert_domain)
        else:
            response = acm.request_certificate(DomainName=cert_domain, SubjectAlternativeNames=alt_names)
    message = cert_domain + " certificate requested, admin approval required"
    if not check_mode:
        cert_arn = response.get('CertificateArn', '')
        acm.add_tags_to_certificate(CertificateArn=cert_arn, Tags=[{'Key': 'Name', 'Value': name},])
    else:
        cert_arn = "Check mode only, nothing created yet"
    return message, cert_arn

def getNameTag(acm, cert_arn):
    response = acm.list_tags_for_certificate(CertificateArn=cert_arn)
    tags = response['Tags']
    for tag in tags:
        if tag['Key'] == "Name":
            return tag['Value']
    return None

# for the bug in earlier code, add tag if the cert exists but does not
# contain the tag
def get_cert_arn_with_empty_tag(acm, existing_certs, cert_domain):
    for cert in existing_certs:
        cert_arn = cert['CertificateArn']
        nameTag = getNameTag(acm, cert_arn)
        if nameTag is None and cert['DomainName'] == cert_domain:
            return cert_arn
    return None

def get_cert_arn(acm, existing_certs, cert_domain, name):
    for cert in existing_certs:
        cert_arn = cert['CertificateArn']
        nameTag = getNameTag(acm, cert_arn)
        if nameTag is not None:
            if cert['DomainName'] == cert_domain and nameTag == name:
                return cert_arn
    return None

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state           = dict(default='present', choices=['present', 'absent']),
            domain_name     = dict(default=None, required=True, type='str'),
            name            = dict(default=None, required=True, type='str'),
            alternate_names = dict(default=None, required=False, type='list')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state           = module.params['state']
    cert_domain     = module.params['domain_name']
    name            = module.params['name']
    alt_names       = module.params['alternate_names']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    acm = boto3.client('acm')

    existing_certs = acm.list_certificates()['CertificateSummaryList']

    # only return the cert if domain matches and the Name tag matches name
    cert_arn = get_cert_arn(acm, existing_certs, cert_domain, name)

    # for previous bug which would not add tags, if cert cant be found
    # with domain and tag try to find the cert with the same domain but
    # missing tag and add the tag.
    if cert_arn is None:
        cert_arn = get_cert_arn_with_empty_tag(acm, existing_certs, cert_domain)
        if cert_arn is not None:
            acm.add_tags_to_certificate(CertificateArn=cert_arn, Tags=[{'Key': 'Name', 'Value': name},])

    message = "Certificate found"
    changed = False

    result = {}

    if state == 'present':
        if cert_arn is None:
            message, cert_arn = request_certificate(acm, cert_domain, alt_names, name, module.check_mode)
            changed = True
    elif state == 'absent':
        if cert_arn is not None:
            changed = True
            message = delete_cert(acm, cert_arn, module.check_mode)
        else:
            changed = False
            message = cert_domain + " certificate does not exist"
            cert_arn = None

    result = dict(message=message,changed=changed,cert_arn=cert_arn,name=name)

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
