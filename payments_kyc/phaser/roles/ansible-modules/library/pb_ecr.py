#!/usr/bin/python

DOCUMENTATION = '''
---
module: pb_ecr
short_description: manage ECR repositories event sources
description:
    - Creates and deletes ECR repositpries
options:
  name:
    description:
      - ECR name
    required: true
  state:
    description:
      - Whether to create or delete ECR
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates a event source
- pb_ecr:
    name: "{{ ecr.name }}"

# Remove a event source
- pb_ecr:
    name: "{{ ecr.name }}"
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def repository_exists(client, name):
    try:
        return client.describe_repositories(repositoryNames=[name])['repositories'][0]
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'RepositoryNotFoundException':
            return None

def list_images(module, client, name, nextToken=False):
    images = []
    try:
        response = client.list_images(repositoryName=name, nextToken=nextToken) if nextToken else client.list_images(repositoryName=name)
        if 'imageIds' in response and isinstance(response['imageIds'], list):
            for image in response['imageIds']:
                images.append(image)
        if 'nextToken' in response and response['nextToken']:
            images.extend(list_images(module, client, name, response['nextToken']))
    except:
        module.fail_json(changed=False,
                         msg="Unknown exception in list_images: {0}".format(sys.exc_info()))
    return images

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(state=dict(default='present', choices=['present', 'absent', 'list']),
                              force=dict(default=False, type='bool'),
                              name=dict(required=True, type='str')))

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state = module.params['state']
    name = module.params['name']
    force = module.params['force']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    aws_ecr = boto3.client('ecr')
    existing_repository = repository_exists(aws_ecr, name)

    if state == 'list':
        module.exit_json(changed=False,
                         msg="List of all images",
                         output=list_images(module, aws_ecr, name))

    result = {}
    if existing_repository is not None:
        if state == 'absent':
            if not module.check_mode:
                aws_ecr.delete_repository(repositoryName=name, force=force)
            result = dict(changed=True, msg="Repository has been deleted")
        else:
            result = dict(changed=False,
                          msg="Repository already exists",
                          output=existing_repository)
    else:
        if state == 'present':
            if not module.check_mode:
                response = aws_ecr.create_repository(repositoryName=name)
            result = dict(changed=True,
                          msg="Repository has been created",
                          output=response['repository'])
        else:
            result = dict(changed=False, msg="Repository doesn't exist")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
