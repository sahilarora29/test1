#!/usr/bin/python

DOCUMENTATION = '''
---
module: iot_rule
short_description: manage IoT Topic Rules
description:
    - Creates and deletes IoT Rules
options:
  name:
    description:
      - Name of the thing type
    required: true
  sql:
    description:
      - The SQL statement used to query the topic
    required: false
  sqlVersion:
    description:
      - The version of the SQL rules engine to use when evaluating the rule
    required: false
  description:
    description:
      - The description of the rule
    required: false
  actions:
    description:
      - The actions associated with the rule
    required: false
  state:
    description:
      - Whether to create or delete the thing type.
    required: true
    default: present
    choices: [ "present", "absent", "disabled" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION
        or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
'''

EXAMPLES = '''
# Creates a rule
- iot_rule:
    name: test_rule
    sql: SELECT * FROM 'topic/events'
    sqlVersion: "2016-03-23"
    actions:
      -
        lambda:
          functionArn: arn
    region: us-west-2

# Remove a rule
- iot_rule:
    name: test_rule
    region: us-west-2
    state: absent
'''

import sys

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def update_rule(iot, rule, params, check_mode):
    updates = []
    enableDisable = False
    replaceRule = False
    name = params['name']

    if params['state'] == 'disabled' and not rule['rule']['ruleDisabled']:
        updates.append(('state', rule['rule']['ruleDisabled'], True))
        enableDisable = True
        if not check_mode:
            iot.disable_topic_rule(ruleName=name)
    elif params['state'] == 'present' and rule['rule']['ruleDisabled']:
        updates.append(('state', rule['rule']['ruleDisabled'], False))
        enableDisable = True
        if not check_mode:
            iot.enable_topic_rule(ruleName=name)

    if rule['rule']['sql'] != params['sql']:
        updates.append(('sql', rule['rule']['sql'], params['sql']))
        replaceRule = True

    if rule['rule']['awsIotSqlVersion'] != params['sqlVersion']:
        updates.append(('sqlVersion', rule['rule']['awsIotSqlVersion'], params['sqlVersion']))
        replaceRule = True

    if rule['rule']['actions'] != params['actions']:
        updates.append(('actions', rule['rule']['actions'], params['actions']))
        replaceRule = True

    newArn = None
    if not check_mode and replaceRule:
        iot.replace_topic_rule(
            ruleName=name,
            topicRulePayload={
                'sql': params['sql'],
                'description': params['description'],
                'actions': params['actions'],
                'ruleDisabled': False,
                'awsIotSqlVersion': params['sqlVersion']
            }
        )
    return dict(changed=replaceRule or enableDisable, output="Rule {} has been updated".format(name), updates=updates, name=name, arn=rule['ruleArn'])

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state               = dict(default='present', choices=['present', 'absent', 'disabled']),
            name                = dict(type='str', required=True),
            sql                 = dict(type='str', required=False),
            sqlVersion          = dict(type='str', required=False),
            description         = dict(type='str', default=''),
            actions             = dict(type='list', default=[])
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state       = module.params['state']
    name        = module.params['name']
    sql         = module.params['sql']
    sqlVersion  = module.params['sqlVersion']
    description = module.params['description']
    actions     = module.params['actions']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    iot = boto3.client('iot')

    rule = None
    description = None
    if not description:
        description = name

    try:
        rule = iot.get_topic_rule(ruleName=name)
        disabled = rule['rule']['ruleDisabled']
    except:
        rule = None

    result = {}
    if rule:
        if state == 'absent':
            if not module.check_mode:
                iot.delete_topic_rule(ruleName=name)
            result = dict(changed=True, output="Rule {} has been deleted".format(name), name=name, arn=rule['ruleArn'])
        else:
            result = update_rule(iot, rule, module.params, module.check_mode)
    else:
        if state == 'present':
            if not module.check_mode:
                response = iot.create_topic_rule(
                    ruleName=name,
                    topicRulePayload={
                        'sql': sql,
                        'description': description,
                        'actions': actions,
                        'ruleDisabled': False,
                        'awsIotSqlVersion': sqlVersion
                    }
                )
                rule = iot.get_topic_rule(ruleName=name)
            result = dict(changed=True, output="Rule {} has been created".format(name), name=name, arn='arn:aws')
        else:
            result = dict(changed=False, output="Rule does not exist", name=name, arn=None)

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
