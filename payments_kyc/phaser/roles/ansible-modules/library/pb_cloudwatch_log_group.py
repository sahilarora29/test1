#!/usr/bin/python

DOCUMENTATION = '''
---
module: cloudwatch_log_group
short_description: Creates a new log group with the specified name. The name of the log group must be unique within a region for an AWS account. You can create up to 500 log groups per account.
description:
    - Creates a new log group with the specified name. The name of the log group must be unique within a region for an AWS account. You can create up to 500 log groups per account.
options:
  name:
    description:
      - The name of the log group.
    required: true
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ 'present', 'absent' ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: '1.5'
'''

EXAMPLES = '''
# Create CW log group
- cloudwatch_log_group:
    name:

# Remove CW log group
- cloudwatch_log_group:
    name:
    state: absent
'''

import sys
import time

try:
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto3 and/or botocore.exceptions required for this module'"
    sys.exit(1)

def find_log_group(logs, name, nextToken=False):
    try:
        if nextToken == False:
            log_groups = logs.describe_log_groups()['logGroups']
        else:
            log_groups = logs.describe_log_groups(nextToken=nextToken)['logGroups']
    except (ClientError, ParamValidationError, MissingParametersError) as e:
        module.fail_json(changed=False, msg='Error describing groups: {0}'.format(e))
    except:
        module.fail_json(changed=False, msg='Unknown exception in describe_log_groups: {0}'.format(sys.exc_info()))

    for group in log_groups:
        if group['logGroupName'] == name: return group

    if 'nextToken' in log_groups and logs_groups['nextToken']: return find_log_group(name, logs_groupsp['nextToken'])
    return None

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state     = dict(default='present', choices=['present', 'absent']),
            name      = dict(required=True, type='str'),
            retention = dict(type='int')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
    if region:
        logs = boto3_conn(module, conn_type='client', resource='logs', region=region, endpoint=ec2_url, **aws_connect_kwargs)
    else:
        module.fail_json(msg='region must be specified')

    existing_log_group = find_log_group(logs, module.params['name'])
    #module.fail_json(changed=False, msg='Existing', var=existing_log_group)

    result = {}

    if module.params['state'] == 'present':
        if not existing_log_group:
            try:
                if not module.check_mode:
                    logs.create_log_group(logGroupName=module.params['name'])
                    if 'retention' in module.params and module.params['retention']:
                        logs.put_retention_policy(logGroupName=module.params['name'],
                                                  retentionInDays=module.params['retention'])
                result = dict(changed=True, output='Cloudwatch log group has been created')
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                if e.message and 'ResourceAlreadyExistsException' in e.message:
                    result = dict(changed=False, output='Cloudwatch log group already exists')
                else:
                    module.fail_json(changed=False, msg='Error creating CW log group: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg='Unknown exception in create_log_group: {0}'.format(sys.exc_info()))
        if existing_log_group:
            if 'retention' in module.params and module.params['retention']:
                if ('retentionInDays' not in existing_log_group) or ('retentionInDays' in existing_log_group and existing_log_group['retentionInDays'] != module.params['retention']):
                    try:
                        if not module.check_mode:
                                logs.put_retention_policy(logGroupName=module.params['name'],
                                                          retentionInDays=module.params['retention'])
                        result = dict(changed=True, output='Cloudwatch log group retention policy has been updated')
                    except (ClientError, ParamValidationError, MissingParametersError) as e:
                        module.fail_json(changed=False, msg='Error applying retention policy on log group: {0}'.format(e))
                    except:
                        module.fail_json(changed=False, msg='Unknown exception in put_retention_policy: {0}'.format(sys.exc_info()))
                else:
                    result = dict(changed=False, output='Cloudwatch log group is up-to-date')
            else:
                if 'retentionInDays' in existing_log_group:
                    try:
                        if not module.check_mode:
                                logs.delete_retention_policy(logGroupName=module.params['name'])
                        result = dict(changed=True, output='Cloudwatch log group retention policy has been removed')
                    except (ClientError, ParamValidationError, MissingParametersError) as e:
                        module.fail_json(changed=False, msg='Error removing retention policy on log group: {0}'.format(e))
                    except:
                        module.fail_json(changed=False, msg='Unknown exception in delete_retention_policy: {0}'.format(sys.exc_info()))
                else:
                    result = dict(changed=False, output='Cloudwatch log group is up-to-date')
    elif module.params['state'] == 'absent':
        try:
            if not module.check_mode:
                logs.delete_log_group(logGroupName=module.params['name'])
            result = dict(changed=True, output='Cloudwatch log group has been deleted')
        except (ClientError, ParamValidationError, MissingParametersError) as e:
             module.fail_json(changed=False, msg='Error deleting CW log group: {0}'.format(e))
        except:
            module.fail_json(changed=False, msg='Unknown exception in delete_log_group: {0}'.format(sys.exc_info()))

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
