#!/usr/bin/python

DOCUMENTATION = '''
---
module: elbv2_listener
short_description: Adds, updates or deletes lambda s3 event notifications
description:
    - Adds, updates or deletes lambda s3 event notifications.
options:
  load_balancer_arn:
    description:
      - The Amazon Resource Name (ARN) of the load balancer.
    required: true
  protocol:
    description:
      - The Amazon Resource Name (ARN) of the load balancer.
    required: true
  port:
    description:
      - The port on which the load balancer is listening.
    required: true
  ssl_policy:
    description:
      - The security policy that defines which ciphers and protocols are supported. The default is the current predefined security policy.
    required: true
  certificates:
    description:
      - The SSL server certificate. You must provide exactly one certificate if the protocol is HTTPS.
    required: true
  default_actions:
    description:
      - The default actions for the listener.
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new ELB v2 target group
- elbv2_listener:
    load_balancer_arn: "{{ appELBArn }}"
    protocol: "{{ protocol }}"
    port: "{{ port }}"
    ssl_policy: "{{ sslPolicy if (sslPolicy is defined and protocol == 'HTTPS') else omit }}"
    certificates: "{{ [{ 'CertificateArn': sslCertificateArn }] }}"
    default_actions:
      - Type: "forward"
        TargetGroupArn: "{{ someDefaultTargetGroupArn }}"
    region: "{{ region }}"

# Remove ELB v2 target group
- elbv2_listener:
    load_balancer_arn: "{{ appELBArn }}"
    port: "{{ port }}"
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def check_updates(old, new):
    updates = []

    if old['Protocol'] == 'HTTPS' and new['Protocol'] == 'HTTPS':
        if 'SslPolicy' in old and 'SslPolicy' in new:
            if old['SslPolicy'] != new['SslPolicy']: updates.append(('SslPolicy', old['SslPolicy'], new['SslPolicy']))
        elif 'SslPolicy' in old and 'SslPolicy' not in new:
            updates.append(('SslPolicy', old['SslPolicy'], 'None'))
        elif 'SslPolicy' not in old and 'SslPolicy' in new:
            updates.append(('SslPolicy', 'None', new['SslPolicy']))
        for old_c in old['Certificates']:
            for new_c in new['Certificates']:
                if old_c['CertificateArn'] != new_c['CertificateArn']:
                    updates.append(('Certificates', old['Certificates'], new['Certificates']))

    if old['Protocol'] == 'HTTP' and new['Protocol'] == 'HTTPS':
        if 'SslPolicy' in new: updates.append(('SslPolicy', 'None', new['SslPolicy']))
        updates.append(('Certificates', 'None', new['Certificates']))

    if old['Protocol'] == 'HTTPS' and new['Protocol'] == 'HTTP':
        if 'SslPolicy' in old: updates.append(('SslPolicy', old['SslPolicy'], 'None'))
        updates.append(('Certificates', old['Certificates'], 'None'))

    for old_da in old['DefaultActions']:
        for new_da in new['DefaultActions']:
            if old_da['Type'] == new_da['Type'] and old_da['TargetGroupArn'] != new_da['TargetGroupArn']:
                updates.append(('DefaultActions', old['DefaultActions'], new['DefaultActions']))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state             = dict(default='present', choices=['present', 'absent']),
            load_balancer_arn = dict(required=True, type='str'),
            protocol          = dict(required=True, type='str'),
            port              = dict(required=True, type='int'),
            ssl_policy        = dict(type='str', default=""),
            certificates      = dict(type='list'),
            default_actions   = dict(required=True, type='list')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
    if region:
        elbv2 = boto3_conn(module, conn_type='client', resource='elbv2', region=region, endpoint=ec2_url, **aws_connect_kwargs)
    else:
        module.fail_json(msg="region must be specified")

    # check for existing group
    existing_listener = None
    try:
        for listener in elbv2.describe_listeners(LoadBalancerArn=module.params['load_balancer_arn'])['Listeners']:
            if listener['Port'] == module.params['port']:
                existing_listener = listener
                break
    except ClientError as e:
        if not e.message or 'ListenerNotFound' not in e.message:
            raise e
    except:
        module.fail_json(changed=False, msg="Unknown exception in describe_listeners: {0}".format(sys.exc_info()))

    new_listener = dict(Protocol=module.params['protocol'],
                        Port=module.params['port'],
                        DefaultActions=module.params['default_actions'])

    if module.params['protocol'] == 'HTTPS':
        if 'ssl_policy' in module.params and module.params['ssl_policy']:
            new_listener.update(SslPolicy=module.params['ssl_policy'])
        if 'certificates' in module.params and module.params['certificates']:
            new_listener.update(Certificates=module.params['certificates'])
        else:
            module.fail_json(msg='Error: "certificates" is required when protocol is HTTPS'.format(e))

    result = {}

    if existing_listener:
        if module.params['state'] == 'present':
            updates = check_updates(existing_listener, new_listener)
            if updates:
                listener = "CHECK_MODE"
                if not module.check_mode:
                    listener = elbv2.modify_listener(ListenerArn=existing_listener['ListenerArn'], **new_listener)['Listeners'][0]
                result = dict(changed=False,
                              output="ELB v2 listener was updated",
                              listener=listener,
                              updates=updates)
            else:
                result = dict(changed=False,
                              output="ELB v2 listener is up-to-date",
                              listener=existing_listener)
        elif module.params['state'] == 'absent':
            try:
                if not module.check_mode:
                    elbv2.delete_listener(ListenerArn=existing_listener['TargetGroupArn'])
                result = dict(changed=True, output="ELB v2 listener was deleted")
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in deleting ELB v2 listener: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in delete_listener: {0}".format(sys.exc_info()))
    else:
        if module.params['state'] == 'present':
            try:
                listener = "CHECK_MODE"
                if not module.check_mode:
                    listener = elbv2.create_listener(LoadBalancerArn=module.params['load_balancer_arn'], **new_listener)['Listeners'][0]
                result = dict(changed=True,
                              output="ELB v2 listener was created",
                              listener=listener)
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in creating ELB v2 listener: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in create_listener: {0}".format(sys.exc_info()))
        elif module.params['state'] == 'absent':
            result = dict(changed=False, output="ELB v2 listener doesn\'t exist. Nothing to remove")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
