#!/usr/bin/python

DOCUMENTATION = '''
---
module: aws_account
short_description: Get AWS Account details
description:
     - Get AWA Account details including id
version_added: "1.1"
options:
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: us-west-2
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"

author: Harpreet Singh
extends_documentation_fragment: aws
'''

EXAMPLES = '''
# Basic task example
tasks:
- name: get aws account id
  aws_account_id:
    region: us-east-1
  register: awsAccount
'''

import json
import time

try:
    import boto.iam
    HAS_BOTO = True
except ImportError:
    HAS_BOTO = False

def main():
    argument_spec = ec2_argument_spec()

    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True
    )

    if not HAS_BOTO:
        module.fail_json(msg='boto required for this module')

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)
    conn = boto.iam.connect_to_region(region)
    user = conn.get_user()

    result = {}
    result = dict(changed=False, user=user, accountId=user.arn.split("::",1)[1].split(":")[0])

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
