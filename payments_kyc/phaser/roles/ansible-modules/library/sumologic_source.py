#!/usr/bin/python

DOCUMENTATION = '''
---
module: sumologic_source
short_description: manage lambda event sources
description:
    - Creates and deletes Lambda Event Sources
options:
  name:
    description:
      - Source name
    required: true
  collector_id:
    description:
      - Collector ID
    required: true
  source_type:
    description:
      - Source type
    required: true
    default: HTTP
    choices: [ "HTTP" ]
  message_per_request:
    description:
      - Enable One message Per Request
    type: boolean
  category:
    description:
      - Source category
    required: true
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
'''

EXAMPLES = '''
# Creates a source
- sumologic_source:
    name: source_name
    username: username/access_id
    password: username/access_key
    collector_id: collector_id
    category: cetegory_name

# Remove a source
- sumologic_source:
    name: source_name
    username: username/access_id
    password: username/access_key
    collector_id: collector_id
    category: cetegory_name
    state: absent
'''

import sys
import time
import json

try:
    from sumologic import SumoLogic
except ImportError:
    print "failed=True msg='sumologic-sdk python package required for this module'"
    sys.exit(1)

def compare_configuration(existing, new):
    updates = []

    if existing['name'] != new['name']:
        updates.append(('name', existing['name'], new['name']))
    if existing['collectorType'] != new['collectorType']:
        updates.append(('collectorType', existing['collectorType'], new['collectorType']))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state               = dict(default='present', choices=['present', 'absent', 'list']),
            name                = dict(type='str', required=True),
            collector_id        = dict(type='str', required=True),
            source_type         = dict(default='HTTP', choices=['HTTP']),
            message_per_request = dict(type='bool', default=True),
            category            = dict(type='str', default=''),
            username            = dict(type='str', required=True),
            password            = dict(type='str', required=True)
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state               = module.params['state']
    name                = module.params['name']
    collector_id        = module.params['collector_id']
    source_type         = module.params['source_type']
    username            = module.params['username']
    password            = module.params['password']
    message_per_request = module.params['message_per_request']
    category            = module.params['category']

    try:
        sumo = SumoLogic(username, password)
    except:
        module.fail_json(changed=False, msg="Unable to get sources")

    try:
        sources = sumo.sources(collector_id)
    except:
        sources = list()

    # id: #
    # name: myname
    # sourceType: HTTP
    # alive: true

    if state == 'list':
        module.exit_json(dict(changed=False, output=sources))

    result = {}
    existing_source = dict()
    new_source = dict(name=name, sourceType=source_type, messagePerRequest=message_per_request, category=category)

    if len(sources) > 0:
        for source in sources:
            if source['name'] == name and source['sourceType'] == source_type and source['messagePerRequest'] == message_per_request:
                existing_source = source

    if existing_source:
        if state == 'present':
            result = dict(changed=False, msg="Source already exists", output=dict(source=existing_source))
        elif state == 'absent':
            if not module.check_mode:
                try:
                    sumo.delete_source(collector_id, existing_source)
                except:
                    module.fail_json(changed=False, msg="Unable to delete source")
            result = dict(changed=True, msg="Source has been deleted")
    else:
        if state == 'present':
            if not module.check_mode:
                try:
                    collector_verbose, etag = sumo.collector(collector_id)
                    headers = { 'If-Match': etag.strip("\\\"") }
                    r = sumo.post('/collectors/' + str(collector_id) + '/sources', dict(source=new_source))
                except:
                    module.fail_json(changed=False, msg="Unable to create source: {}".format(sys.exc_info()))
            result = dict(changed=True, msg="Source has been created", output=json.loads(r.text))

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
