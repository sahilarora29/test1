#!/usr/bin/python

DOCUMENTATION = '''
---
module: elbv2
short_description: Adds, updates or deletes ELB v2
description:
    - Adds, updates or deletes ELB v2
options:
  name:
    description:
      - ELB name
    required: true
  subnets:
    description:
      - List of subnets
    required: true
  security_groups:
    description:
      - Name of the bucket
    required: true
  scheme:
    description:
      - ELB scheme: internet-facing or internal
    required: false
  tags:
    description:
      - tags in key:value format
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new ELB v2
- elbv2:
    name:
    subnets:
    security_groups:
      - "{{ group1 }}"
    scheme: "internet-facing"
    tags:
      - Name: new_name
    attributes:
      - a: b
      - c: e

# Remove ELB v2 permission
- elbv2:
    name:
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def check_attributes_updates(old, new):
    updates = []
    if 'Attributes' in old and 'Attributes' in new:
        for oa in old['Attributes']:
            for na in new['Attributes']:
                if oa['Key'] == na['Key'] and oa['Value'] != na['Value']:
                    updates.append(('Attribute/' + oa['Key'], oa['Value'], na['Value']))
                    old[:] = [x for x in old if x != oa]
                    new[:] = [x for x in new if x != na]
        for oa in [x for x in old['Attributes'] if x not in new['Attributes']]:
            updates.append(('Attribute/' + oa['Key'], oa['Value'], 'None'))
        for na in [x for x in new['Attributes'] if x not in old['Attributes']]:
            updates.append(('Attribute/' + na['Key'], 'None', na['Value']))
    elif 'Attributes' not in old and 'Attributes' in new:
        updates.append(('Attributes', 'None', new['Attributes']))
    elif 'Attributes' in old and 'Attributes' not in new:
        updates.append(('Attributes', old['Attributes'], 'None'))

    return updates

def check_subnet_updates(old, new):
    updates = []
    if 'Subnets' in old and 'Subnets' in new:
        if cmp(sorted(old['Subnets']), sorted(new['Subnets'])) in [1,-1]:
            updates.append(('Subnets', old['Subnets'], new['Subnets']))

    return updates

def check_sg_updates(old, new):
    updates = []
    if 'SecurityGroups' in old and 'SecurityGroups' in new:
        if cmp(sorted(old['SecurityGroups']), sorted(new['SecurityGroups'])) in [1,-1]:
            updates.append(('SecurityGroups', old['SecurityGroups'], new['SecurityGroups']))
    elif 'SecurityGroups' in old and 'SecurityGroups' not in new:
        updates.append(('SecurityGroups', old['SecurityGroups'], 'None'))
    elif 'SecurityGroups' not in old and 'SecurityGroups' in new:
        updates.append(('SecurityGroups', 'None', new['SecurityGroups']))

    return updates


def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        state               = dict(default='present', choices=['present', 'absent']),
        name                = dict(required=True, type='str'),
        subnets             = dict(required=True, type='list'),
        security_groups     = dict(type='list'),
        scheme              = dict(default='internet-facing', type='str'),
        tags                = dict(type='list'),
        attributes          = dict(type='list')
    ))

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
     
    if not region:
        module.fail_json(msg="region must be specified")

    elbv2 = boto3_conn(module, conn_type='client', resource='elbv2', region=region, endpoint=ec2_url, **aws_connect_kwargs)

    new_elb = dict(Name=module.params['name'], Subnets=module.params['subnets'], Scheme=module.params['scheme'])
    if 'security_groups' in module.params and module.params['security_groups']:
        new_elb['SecurityGroups'] = module.params['security_groups']
    if 'tags' in module.params and module.params['tags']:
        tags = []
        for tag in module.params['tags']:
            for key, value in tag.iteritems():
                tags.append(dict(Key=key, Value=value))
        if tags: new_elb['Tags'] = tags

    new_elb_attributes = []
    if 'attributes' in module.params and module.params['attributes']:
        for attribute in module.params['attributes']:
            for key, value in attribute.iteritems():
                new_elb_attributes.append(dict(Key=key, Value=value))

    existing_elb = {}
    try:
        existing_elb = elbv2.describe_load_balancers(Names=[module.params['name']])['LoadBalancers'][0]
    except ClientError, e:
        if not e.message or 'LoadBalancerNotFound' not in e.message:
            raise e
    except:
        module.fail_json(msg='Exception in describe_load_balancers: {}'.format(sys.exc_info()))

    existing_elb_attributes = []
    if existing_elb:
        try:
            existing_elb_attributes = elbv2.describe_load_balancer_attributes(LoadBalancerArn=existing_elb['LoadBalancerArn'])['Attributes']
        except:
            module.fail_json(msg='Exception in describe_load_balancer_attributes: {}'.format(sys.exc_info()))

    if module.params['state'] == 'present':
        if existing_elb:
            attr_updates = check_attributes_updates(existing_elb, new_elb)
            subnet_updates = check_subnet_updates(existing_elb, new_elb)
            sg_updates = check_sg_updates(existing_elb, new_elb)

            result = dict(changed=True, output="ELBv2 was updated succesfully", elbv2=existing_elb, updates=attr_updates + subnet_updates + sg_updates)
            if attr_updates:
                try:
                    output_elb_attrs = "CHECK_MODE"
                    if not module.check_mode:
                        output_elb_attrs = elbv2.modify_load_balancer_attributes(LoadBalancerArn=existing_elb['LoadBalancerArn'], Attributes=new_elb_attributes)['Attributes']
                    result['attributes'] = output_elb_attrs
                except (ClientError, ParamValidationError, MissingParametersError) as e:
                    module.fail_json(msg='Error modifying ELB attributes: {0}'.format(e))

            if subnet_updates:
                try:
                    output_elb_subnets = "CHECK_MODE"
                    if not module.check_mode:
                        output_elb_subnets = elbv2.set_subnets(LoadBalancerArn=existing_elb['LoadBalancerArn'], Subnets=new_elb['Subnets'])['AvailabilityZones']
                    result['availability_zones'] = output_elb_subnets
                except (ClientError, ParamValidationError, MissingParametersError) as e:
                    module.fail_json(msg='Error modifying ELBv2 subnets: {0}'.format(e))

            if sg_updates:
                try:
                    output_elb_sg = "CHECK_MODE"
                    if not module.check_mode:
                        sg = new_elb['SecurityGroups'] if 'SecurityGroups' in new_elb else []
                        output_elb_sg = elbv2.set_security_groups(LoadBalancerArn=existing_elb['LoadBalancerArn'], SecurityGroups=sg)['SecurityGroupIds']
                    result['security_group_ids'] = output_elb_sg
                except (ClientError, ParamValidationError, MissingParametersError) as e:
                    module.fail_json(msg='Error modifying ELBv2 secirity groups: {0}'.format(e))

            if not attr_updates and not subnet_updates and not sg_updates:
                result = dict(changed=False, output="ELBv2 attributes are up to date", elbv2=existing_elb, attributes=existing_elb_attributes)
        else:
            try:
                output_elb = output_elb_attrs = "CHECK_MODE"
                result = dict(changed=True, output="ELBv2 was created succesfully")
                if not module.check_mode:
                    result['elbv2'] = elbv2.create_load_balancer(**new_elb)['LoadBalancers'][0]
                    if new_elb_attributes:
                        result['attributes'] = elbv2.modify_load_balancer_attributes(LoadBalancerArn=output_elb['LoadBalancerArn'], Attributes=new_elb_attributes)['Attributes']
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                module.fail_json(msg='Error creating new ELBv2 or modifying attributes: {0}'.format(e))
    else:
        if existing_elb:
            try:
                if not module.check_mode:
                    output = elbv2.delete_load_balancer(LoadBalancerArn=existing_elb['LoadBalancerArn'])
                result = dict(changed=True, output="ELBv2 was removed")
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                module.fail_json(msg='Error removing ELBv2: {0}'.format(e))
        else:
            result = dict(changed=False, output="Nothing to remove. ELBv2 does not exist")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
