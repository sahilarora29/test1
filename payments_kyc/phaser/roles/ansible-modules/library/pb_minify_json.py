#!/usr/bin/env python

DOCUMENTATION='''
---
module: minify_json
short_description: Minify JSON file
author: Harpreet Singh
options:
    src:
        required: true
        description:
            - Location of the JSON file to minifiy
    dest:
        required: false
        description:
            - Where to save minified JSON, defaults to tmp/mini.json
'''

EXAMPLES='''
- name: minify json
  minify_json: src=pb_vpc.json
'''

import json
from ansible.module_utils.basic import *

def main():

    module = AnsibleModule(
        argument_spec = dict(
            src    = dict(required = True),
            dest   = dict(required = False)
        ),
        supports_check_mode = True
    )

    src   = module.params['src']
    dest  = module.params['dest']

    if module.check_mode:
        module.exit_json(
            path = src,
            changed = True
        )

    if dest is None:
        if not os.path.exists("tmp"):
            os.mkdir("tmp")
        dest = "tmp/mini.json"

    src_json_file = open(src, 'r')
    src_json = json.load(src_json_file)

    dest_json_file = open(dest, 'w')
    json.dump(src_json, dest_json_file)

    module.exit_json(
        path = dest,
        changed = True
    )

main()
