#!/usr/bin/python

DOCUMENTATION = '''
---
module: elbv2_target_group
short_description: Adds, updates or deletes lambda s3 event notifications
description:
    - Adds, updates or deletes lambda s3 event notifications.
options:
  name:
    description:
      - The name of the target group.
    required: true
  protocol:
    description:
      - The protocol to use for routing traffic to the targets.
    required: true
  port:
    description:
      - The port on which the targets receive traffic. This port is used unless you specify a port override when registering the target.
    required: true
  vpc_id:
    description:
      - The identifier of the virtual private cloud (VPC).
    required: true
  healthcheck_protocol:
    description:
      - The protocol the load balancer uses when performing health checks on targets. The default is the HTTP protocol
  healthcheck_port:
    description:
      - The port the load balancer uses when performing health checks on targets. The default is traffic-port , which indicates the port on which each target receives traffic from the load balancer
  healthcheck_path:
    description:
      - The ping path that is the destination on the targets for health checks. The default is /.
  healthcheck_interval_seconds:
    description:
      - The approximate amount of time, in seconds, between health checks of an individual target. The default is 30 seconds.
  healthcheck_timeout_seconds:
    description:
      - The amount of time, in seconds, during which no response from a target means a failed health check. The default is 5 seconds.
  healthy_threshold_count:
    description:
      - The number of consecutive health checks successes required before considering an unhealthy target healthy. The default is 5.
  unhealthy_threshold_count:
    description:
     - The number of consecutive health check failures required before considering a target unhealthy. The default is 2.
  matcher:
    description:
      - The HTTP codes to use when checking for a successful response from a target. The default is 200.
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new ELB v2 target group
- elbv2_target_group:
    name:
    protocol:
    port:
    vpc_id:

# Remove ELB v2 target group
- elbv2_target_group:
    name:
    state: absent
'''

import sys
import time

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z

def recreation_required(old, new):
    for v in ['protocol', 'port', 'vpc_id' ]:
        if v in old and v in new:
            if old[v] != new[v]: return True
    return False

def check_updates(old, new):
    updates = []
    options = ['HealthCheckProtocol',
               'HealthCheckPort', 'HealthCheckPath', 'HealthCheckIntervalSeconds',
               'HealthCheckTimeoutSeconds', 'HealthyThresholdCount']

    for v in options:
        if v in old and v in new:
            if old[v] != new[v]:
                updates.append((v, old[v], new[v]))
        elif v not in old and v in new:
            updates.append((v, 'None', new[v]))
        elif v in old and v not in new:
            updates.append((v, old[v], 'None'))

    if 'Matcher' in old and 'Matcher' in new:
        if cmp(old['Matcher'], new['Matcher']) in [1, -1]:
            updates.append(('Matcher', old['Matcher'], new['Matcher']))
    elif 'Matcher' not in old and 'Matcher' in new:
        updates.append(('Matcher', 'None', new['Matcher']))
    elif 'Matcher' in old and 'Matcher' not in new:
        updates.append(('Matcher', old['Matcher'], 'None'))

    return updates

def check_attributes(old, new):
    updates = []

    if old and new:
        old_attributes=old
        new_attributes=new
        attr_updates = []
        for olda in old_attributes[:]:
            for newa in new_attributes[:]:
                if olda['Key'] == newa['Key']:
                    if olda['Value'] != newa['Value']:
                        attr_updates.append((olda, newa))
                    old_attributes.remove(olda)
                    new_attributes.remove(newa)
        for old_attribute in old_attributes:
            attr_updates.append((old_attribute, 'None'))
        for new_attribute in new_attributes:
            attr_updates.append(('None', new_attribute))
        if attr_updates:
            updates.append(('Attributes', attr_updates))
    elif not old and new:
        updates.append(('Attributes', 'None', new['Attributes']))
    elif old and not new:
        updates.append(('Attributes', old['Attributes'], 'None'))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state                         = dict(default='present', choices=['present', 'absent']),
            name                          = dict(required=True, type='str'),
            protocol                      = dict(required=True, type='str', choices=['HTTP', 'HTTPS']),
            port                          = dict(required=True, type='int'),
            vpc_id                        = dict(required=True, type='str'),
            #listener_arn                  = dict(required=True, type='str'),
            healthcheck_protocol          = dict(type='str', choices=['HTTP', 'HTTPS']),
            healthcheck_port              = dict(type='str', default='traffic-port'),
            healthcheck_path              = dict(type='str'),
            healthcheck_interval_seconds  = dict(type='int', default=30),
            healthcheck_timeout_seconds   = dict(type='int', default=5),
            healthy_threshold_count       = dict(type='int', default=5),
            unhealthy_threshold_count     = dict(type='int', default=2),
            matcher                       = dict(type='dict', default=dict(HttpCode="200")),
            deregistration_delay          = dict(type='str', default="300"),
            stickiness_enabled            = dict(type='str', default="false"),
            stickiness_type               = dict(type='str', default="lb_cookie"),
            stickiness_lb_cookie_duration = dict(type='str', default="86400"),
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
    if region:
        elbv2 = boto3_conn(module, conn_type='client', resource='elbv2', region=region, endpoint=ec2_url, **aws_connect_kwargs)
    else:
        module.fail_json(msg="region must be specified")

    # check for existing group
    existing_group = None
    try:
        existing_group = elbv2.describe_target_groups(Names=[module.params['name']])['TargetGroups'][0]
    except ClientError as e:
        if not e.message or 'TargetGroupNotFound' not in e.message:
            raise e
    except:
        module.fail_json(changed=False, msg="Unknown exception in describe_target_groups: {0}".format(sys.exc_info()))

    existing_attributes = None
    if existing_group:
        try:
            existing_attributes = elbv2.describe_target_group_attributes(TargetGroupArn=existing_group['TargetGroupArn'])['Attributes']
        except (ClientError, ParamValidationError, MissingParametersError) as e:
             module.fail_json(msg='Error in describing target group: {0}'.format(e))
        except:
            module.fail_json(changed=False, msg="Unknown exception in describe_target_group_attributes: {0}".format(sys.exc_info()))

    new_group = dict(Name=module.params['name'],
                     Protocol=module.params['protocol'],
                     Port=module.params['port'],
                     VpcId=module.params['vpc_id'])

    new_group_parameters = {}
    if 'healthcheck_protocol' in module.params and module.params['healthcheck_protocol']:
        new_group_parameters.update(HealthCheckProtocol=module.params['healthcheck_protocol'])
    if 'healthcheck_port' in module.params and module.params['healthcheck_port']:
        new_group_parameters.update(HealthCheckPort=module.params['healthcheck_port'])
    if 'healthcheck_path' in module.params and module.params['healthcheck_path']:
        new_group_parameters.update(HealthCheckPath=module.params['healthcheck_path'])
    if 'healthcheck_interval_seconds' in module.params and module.params['healthcheck_interval_seconds']:
        new_group_parameters.update(HealthCheckIntervalSeconds=module.params['healthcheck_interval_seconds'])
    if 'healthcheck_timeout_seconds' in module.params and module.params['healthcheck_timeout_seconds']:
        new_group_parameters.update(HealthCheckTimeoutSeconds=module.params['healthcheck_timeout_seconds'])
    if 'healthy_threshold_count' in module.params and module.params['healthy_threshold_count']:
        new_group_parameters.update(HealthyThresholdCount=module.params['healthy_threshold_count'])
    if 'unhealthy_threshold_count' in module.params and module.params['unhealthy_threshold_count']:
        new_group_parameters.update(UnhealthyThresholdCount=module.params['unhealthy_threshold_count'])
    if 'matcher' in module.params and module.params['matcher']:
        new_group_parameters.update(Matcher=module.params['matcher'])

    new_attributes = []
    new_attributes.append(dict(Key="deregistration_delay.timeout_seconds", Value=module.params['deregistration_delay']))
    new_attributes.append(dict(Key="stickiness.enabled", Value=module.params['stickiness_enabled']))
    new_attributes.append(dict(Key="stickiness.type", Value=module.params['stickiness_type']))
    new_attributes.append(dict(Key="stickiness.lb_cookie.duration_seconds", Value=module.params['stickiness_lb_cookie_duration']))

    result = {}

    if existing_group:
        if module.params['state'] == 'present':
            # Check for updates
            if recreation_required(existing_group, new_group):
                target_arn = "CHECK_MODE"
                if not module.check_mode:
                    try:
                        elbv2.delete_target_group(TargetGroupArn=existing_group['TargetGroupArn'])
                        target_arn = elbv2.create_target_group(**merge_two_dicts(new_group, new_group_parameters))['TargetGroups'][0]['TargetGroupArn']
                        if new_attributes:
                            elbv2.modify_target_group_attributes(TargetGroupArn=target_arn,
                                                                 Attributes=new_attributes)
                    except (ClientError, ParamValidationError, MissingParametersError) as e:
                         module.fail_json(msg='Error recreating ELBv2 target group: {0}\nException: {1}'.format(e, sys.exc_info()))
                    except:
                        module.fail_json(changed=False, msg="Unknown exception: {0}".format(sys.exc_info()))
                result = dict(changed=False,
                             output="Target group aws recreated",
                             arn=target_arn)
            else:
                updates = check_updates(existing_group, new_group_parameters)
                attr_updates = check_attributes(existing_attributes, new_attributes)
                if not updates and not attr_updates:
                    result = dict(changed=False,
                                  output="Target group is up to date",
                                  arn=existing_group['TargetGroupArn'])
                else:
                    output = ""
                    try:
                        if updates:
                            if not module.check_mode:
                                target_arn = elbv2.modify_target_group(TargetGroupArn=existing_group['TargetGroupArn'], **new_group_parameters)
                            output = "Target group was updated"
                        if attr_updates:
                            if not module.check_mode:
                                elbv2.modify_target_group_attributes(TargetGroupArn=existing_group['TargetGroupArn'], Attributes=new_attributes)
                            output = output + ". Target group attributes were updated"
                    except (ClientError, ParamValidationError, MissingParametersError) as e:
                         module.fail_json(msg='Error recreating ELBv2 target group: {0}\nException: {1}'.format(e, sys.exc_info()))
                    except:
                        module.fail_json(changed=False, msg="Unknown exception: {0}".format(sys.exc_info()))
                    result = dict(changed=True, output=output, arn=existing_group['TargetGroupArn'])
        elif module.params['state'] == 'absent':
            try:
                if not module.check_mode:
                    elbv2.delete_target_group(TargetGroupArn=existing_group['TargetGroupArn'])
                result = dict(changed=True, output="ELB v2 target group has been deleted")
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in deleting ELB v2 target group: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in delete_target_group: {0}".format(sys.exc_info()))
    else:
        if module.params['state'] == 'present':
            try:
                target_arn="CHECK_MODE"
                if not module.check_mode:
                    target_arn = elbv2.create_target_group(**merge_two_dicts(new_group, new_group_parameters))['TargetGroups'][0]['TargetGroupArn']
                    if new_attributes:
                        elbv2.modify_target_group_attributes(TargetGroupArn=target_arn,
                                                             Attributes=new_attributes)
                result = dict(changed=True,
                              output="ELB v2 target group has been created",
                              arn=target_arn)
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in creating ELB v2 target group: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in create_target_group: {0}".format(sys.exc_info()))
        elif module.params['state'] == 'absent':
            result = dict(changed=False, output="ELB v2 doesnt exist. Nothing to remove")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
