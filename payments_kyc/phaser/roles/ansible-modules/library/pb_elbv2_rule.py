#!/usr/bin/python

DOCUMENTATION = '''
---
module: elbv2_rule
short_description: Adds, updates or deletes lambda s3 event notifications
description:
    - Adds, updates or deletes lambda s3 event notifications.
options:
  listener_arn:
    description:
      - The Amazon Resource Name (ARN) of the listener.
    required: true
  path_pattern:
    description:
      - A path pattern is case sensitive, can be up to 255 characters in length
    required: true
  priority:
    description:
      - The priority for the rule. A listener can't have multiple rules with the same priority.
    required: true
  target_group_arn:
    description:
      -
    required: true
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Create new ELB v2 target group
- elbv2_rule:
    listener_arn:
    path_pattern:
    priority:
    target_group_arn:

# Remove ELB v2 target group
- elbv2_rule:
    listener_arn:
    path_pattern:
    priority:
    target_group_arn:
    state: absent
'''

import sys
import time

try:
    import boto3
    from botocore.exceptions import ClientError, ParamValidationError, MissingParametersError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def _right_has_values_of_left(left, right):
    # Make sure the values are equivalent for everything left has
    for k, v in left.items():
        if not ((not v and (k not in right or not right[k])) or (k in right and v == right[k])):
            # We don't care about list ordering because ECS can change things
            if isinstance(v, list) and k in right:
                left_list = v
                right_list = right[k] or []

                if len(left_list) != len(right_list):
                    return False

                for list_val in left_list:
                    if list_val not in right_list:
                        return False
            else:
                return False

    # Make sure right doesn't have anything that left doesn't
    for k, v in right.items():
        if v and k not in left:
            return False

    return True

def check_updates(old, new):
    updates = []

    if int(old['Priority']) != int(new['Priority']):
        updates.append(('Priority', int(old['Priority']), int(new['Priority'])))

    for old_condition in old['Conditions']:
        for new_condition in new['Conditions']:
            if old_condition['Field'] == new_condition['Field']:
                if cmp(sorted(old_condition['Values']), sorted(new_condition['Values'])) in [1,-1]:
                    updates.append(('Conditions', old['Conditions'], new['Conditions']))

    for old_action in old['Actions']:
        for new_action in new['Actions']:
            if old_action['Type'] == new_action['Type']:
                if old_action['TargetGroupArn'] != new_action['TargetGroupArn']:
                    updates.append(('Actions', old['Actions'], new['Actions']))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        state            = dict(default='present', choices=['present', 'absent']),
        listener_arn     = dict(required=True, type='str'),
        conditions       = dict(required=True, type='list'),
        actions          = dict(reqiured=True, type='list'),
        priority         = dict(type='int')
        # path_pattern     = dict(type='str', default=None),
        # host_header      = dict(type='str', default=None),
        # target_group_arn = dict(required=True, type='str')
    ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
    if not region:
        module.fail_json(msg="region must be specified")

    elbv2 = boto3_conn(module, conn_type='client', resource='elbv2', region=region, endpoint=ec2_url, **aws_connect_kwargs)

    # check for existing rule
    existing_rules = []
    existing_rule = None
    priorities = []
    try:
        for rule in elbv2.describe_rules(ListenerArn=module.params['listener_arn'])['Rules']:
            if rule['IsDefault'] == True: continue
            rule['Priority'] = int(rule['Priority'])
            priorities.append(rule['Priority'])

            found_action = False
            found_condition = False

            for requested_action in module.params['actions']:
                for existing_action in rule['Actions']:
                    if _right_has_values_of_left(requested_action, existing_action):
                        found_action = True

            for requested_condition in module.params['conditions']:
                for existing_condition in rule['Conditions']:
                    if _right_has_values_of_left(requested_condition, existing_condition):
                        found_condition = True

            if found_action or found_condition:
                existing_rules.append(rule)

    except ClientError as e:
        if not e.message or 'RuleNotFound' not in e.message:
            raise e
    except:
        module.fail_json(changed=False, msg="Unknown exception in describe_rules: {0}".format(sys.exc_info()))

    # fail if mutiple rules detected and priority isn't the same
    if len(existing_rules) == 1:
        existing_rule = existing_rules[0]
    elif len(existing_rules) > 1:
        for rule in existing_rules:
            if 'priority' in module.params and module.params['priority'] and rule['Priority'] == module.params['priority']:
                existing_rule = rule
        if not existing_rule:
            module.fail_json(changed=False, msg="More than one existing rule detected. Please provide correct priority", rules=existing_rules)

    # generate new Priority if no any provided
    priority = 1
    if 'priority' in module.params and module.params['priority']:
        priority = module.params['priority']
    elif existing_rule:
        priority = existing_rule['Priority']
    elif len(priorities) > 0:
        priority = int(next(i for i, e in enumerate(sorted(priorities) + [ None ], 1) if i != e))

    new_rule = dict(ListenerArn=module.params['listener_arn'],
                    Conditions=module.params['conditions'],
                    Priority=priority,
                    Actions=module.params['actions'])

    result = {}

    #module.fail_json(msg="Error", var=existing_rule)

    if existing_rule:
        if module.params['state'] == 'present':
            # Check for updates
            updates = check_updates(existing_rule, new_rule)
            new_rule['RuleArn'] = existing_rule['RuleArn']
            if updates:
                try:
                    if not module.check_mode:
                        elbv2.modify_rule(RuleArn=existing_rule['RuleArn'],
                                          Conditions=new_rule['Conditions'],
                                          Actions=new_rule['Actions'])
                        if int(existing_rule['Priority']) != int(new_rule['Priority']):
                            elbv2.set_rule_priorities(RuleArn=existing_rule['RuleArn'], Priority=new_rule['Priority'])
                    result = dict(changed=True, output="ELB v2 rule has been updated", rule=new_rule, updates=updates)
                except (ClientError, ParamValidationError, MissingParametersError) as e:
                     module.fail_json(msg='Error in modifying the ELB v2 rule: {0}'.format(e))
                except:
                    module.fail_json(changed=False, msg="Unknown exception in modify_rule: {0}".format(sys.exc_info()))
            else:
                result = dict(changed=False,
                              output="ELB v2 rule is up-to-date",
                              rule=existing_rule)
        elif module.params['state'] == 'absent':
            try:
                if not module.check_mode:
                    elbv2.delete_rule(RuleArn=existing_rule['RuleArn'])
                result = dict(changed=True, output="ELB v2 rule has been deleted")
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in deleting ELB v2 rule: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in delete_rule: {0}".format(sys.exc_info()))
    else:
        if module.params['state'] == 'present':
            try:
                rule="CHECK_MODE"
                if not module.check_mode:
                    rule = elbv2.create_rule(**new_rule)['Rules'][0]
                result = dict(changed=True,
                              output="ELB v2 listener rule has been created",
                              rule=rule)
            except (ClientError, ParamValidationError, MissingParametersError) as e:
                 module.fail_json(msg='Error in creating ELB v2 rule: {0}'.format(e))
            except:
                module.fail_json(changed=False, msg="Unknown exception in create_rule: {0}".format(sys.exc_info()))
        elif module.params['state'] == 'absent':
            result = dict(changed=False, output="ELB v2 rule doesnt exist. Nothing to remove")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
