#!/usr/bin/python

DOCUMENTATION = '''
---
module: sumologic_collector
short_description: manage lambda event sources
description:
    - Creates and deletes Lambda Event Sources
options:
  name:
    description:
      - Source name
    required: true
  username:
    description:
      - Sumoloigc account username
    required: true
  password:
    description:
      - Sumoloigc account password
    required: true
  collector_type:
    description:
      - Largest number of records Lambda will retrieve from the event source.
    required: true
    default: hosted
    choices: [ "hosted" ]
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: list
    choices: [ "list" ]
    aliases: []
'''

EXAMPLES = '''
# Creates a event source
- sumologic_collector:
    name: "collector-name"
    username: username@domain.com
    password: paswword
    state: list
    collector_type: hosted
  register: sumo_collector
'''

import sys
import time

try:
    from sumologic import SumoLogic
except ImportError:
    print "failed=True msg='sumologic-sdk python package required for this module'"
    sys.exit(1)

def compare_configuration(existing, new):
    updates = []

    if existing['name'] != new['name']:
        updates.append(('name', existing['name'], new['name']))
    if existing['collectorType'] != new['collectorType']:
        updates.append(('collectorType', existing['collectorType'], new['collectorType']))

    return updates

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state          = dict(default='list', choices=['present', 'absent', 'list']),
            name           = dict(type='str', required=True),
            username       = dict(type='str', required=True),
            password       = dict(type='str', required=True),
            collector_type = dict(default='hosted', choices=['hosted']),
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state          = module.params['state']
    name           = module.params['name']
    username       = module.params['username']
    password       = module.params['password']
    collector_type = module.params['collector_type']

    try:
        sumo = SumoLogic(username, password)
    except:
        module.fail_json(changed=False, msg="Unable connect to SumoLogic")

    try:
        collectors = sumo.collectors()
    except:
        module.fail_json(changed=False, msg="Unable to get collectors")

    #new_collector = dict(name=name, collectorType=collector_type.title())
    output = dict()
    existing_collector = dict()

    if state == 'list':
        for collector in collectors:
            if collector['name'] == name and collector['collectorType'] == collector_type.title():
                try:
                    collector_verbose, etag = sumo.collector(collector['id'])
                    output = dict(collector_verbose, etag=etag.strip("\\\""))
                except:
                    module.fail_json(changed=False, msg="Unable to get exact collector")

    if not output:
        module.fail_json(changed=False, msg="Unable to find collector")

    result = dict(changed=False, output=output)

    # result = {}
    # if existing_id:
    #     if state == 'absent':
    #         if not module.check_mode:
    #             sumo.delete_collector(existing_collector)
    #         result = dict(changed=True, output="Collector has been deleted")
    #     else:
    #         if len(updates) > 0:
    #             if not module.check_mode:
    #
    #                 sumo.update_collector(new_collector)
    #             result = dict(changed=True, output=updates)
    #         else:
    #             result = dict(changed=False, output="Collector already exists")
    # else:
    #     if state == 'present':
    #         if not module.check_mode:
    #             sumo.
    #         result = dict(changed=True, output="Collector has been created")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
