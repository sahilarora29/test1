#!/usr/bin/python

DOCUMENTATION = '''
---
module: kinesis
short_description: manage kinesis streams
description:
    - Creates and delets Kinesis streams
options:
  name:
    description:
      - Name of the stream
    required: true
  shard_count:
    description:
      - The number of shards that the stream uses. Required if state is present
  state:
    description:
      - Whether to create or delete stream. When present is specified it will attempt to make an update if shardCount has changed.
    required: true
    default: present
    choices: [ "present", "absent" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
    version_added: "1.5"
'''

EXAMPLES = '''
# Creates a stream
- kinesis:
    name: test_stream
    shartCount: 1

# Remove a stream
- kinesis:
    name: test_stream
    state: absent
'''

import sys
import time

try:
    import boto.kinesis
    import boto.logs
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state      = dict(default='present', choices=['present', 'absent']),
            name       = dict(default=None, required=True, type='str'),
            shard_count= dict(default=1, type='int')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state      = module.params['state']
    name       = module.params['name']
    shard_count= module.params['shard_count']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    kinesis = boto.kinesis.connect_to_region(region)

    try:
        stream = kinesis.describe_stream(name)
    except:
        print sys.exc_info()[0]
        stream = None

    result = {}
    if stream:
        if state == 'absent':
            if not module.check_mode:
                kinesis.delete_stream(name)
            result = dict(changed=True, output="Stream deleted")
        else:
            result = dict(changed=False, output="Stream already exists",
                          arn=stream['StreamDescription']['StreamARN'],
                          status=stream['StreamDescription']['StreamStatus'])
    else:
        if state == 'present':
            if not module.check_mode:
                kinesis.create_stream(name, shard_count)
                result = dict(changed=True, output="Stream created")

                stream = kinesis.describe_stream(name)
                retries = 0

                while stream['StreamDescription']['StreamStatus'] != 'ACTIVE' or retries == 4:
                    time.sleep(5 * (2**retries))
                    retries += 1
                    stream = kinesis.describe_stream(name)

                result['arn'] = stream['StreamDescription']['StreamARN']
                result['status'] = status=stream['StreamDescription']['StreamStatus']
        else:
            result = dict(changed=False, output="Stream doesn't exist")

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
