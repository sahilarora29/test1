#!/usr/bin/python

DOCUMENTATION = '''
---
module: iot_thing_type
short_description: manage IoT Thing Types
description:
    - Creates and deletes Thing Types
options:
  name:
    description:
      - Name of the thing type
    required: true
  description:
    description:
        - The description of the thing type
    required: false
  attributes:
    description:
      - A list of searchable thing attribute names
    required: false
  state:
    description:
      - Whether to create or delete the thing type.
    required: true
    default: present
    choices: [ "present", "absent", "deprecated" ]
    aliases: []
  region:
    description:
      - The AWS region to use. If not specified then the value of the AWS_REGION
        or EC2_REGION environment variable, if any, is used.
    required: true
    default: null
    aliases: ['aws_region', 'ec2_region']
'''

EXAMPLES = '''
# Creates a thing type
- iot_thing_type:
    name: LightBulb
    description: light bulb type
    attributes:
      - wattage
      - model
    region: us-west-2

# Remove a thing type
- iot_thing_type:
    name: LightBulb
    region: us-west-2
    state: absent
'''

import sys

try:
    import boto
    import boto3
    from botocore.exceptions import ClientError
except ImportError:
    print "failed=True msg='boto required for this module'"
    sys.exit(1)

def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
            state               = dict(default='present', choices=['present', 'absent', 'deprecated']),
            name                = dict(type='str', required=True),
            description         = dict(type='str', default=''),
            attributes          = dict(default=[], type='list')
        ),
    )

    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=True)

    state               = module.params['state']
    name                = module.params['name']
    description         = module.params['description']
    attributes          = module.params['attributes']

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    boto3.setup_default_session(region_name=region)
    iot = boto3.client('iot')

    thingType = None
    deprecated = False
    if not description:
        description = name

    try:
        thingType = iot.describe_thing_type(thingTypeName=name)
        deprecated = thingType['thingTypeMetadata']['deprecated']
    except:
        thingType = None

    result = {}
    if thingType:
        if state == 'absent':
            if not module.check_mode:
                iot.delete_thing_type(thingTypeName=name)
            result = dict(changed=True, output="ThingType {} has been deleted".format(name), name=name)
        elif state == 'deprecated':
            if deprecated:
                result = dict(changed=False, output="ThingType {} is already deprecated".format(name), name=name)
            else:
                if not module.check_mode:
                    iot.deprecate_thing_type(thingTypeName=name)
                result = dict(changed=True, output="ThingType {} has been deprecated".format(name), name=name)
        else:
           if deprecated:
               if not module.check_mode:
                   iot.deprecate_thing_type(thingTypeName=name, undoDeprecate=True)
               result = dict(changed=True, output="ThingType {} has been un-deprecated".format(name), name=name)
           else:
               result = dict(changed=False, output="ThingType already exists", name=name, thingType=thingType, deprecated=deprecated)
    else:
        if state == 'present':
            if not module.check_mode:
                response = iot.create_thing_type(
                    thingTypeName=name,
                    thingTypeProperties={
                        'thingTypeDescription': description,
                        'searchableAttributes': attributes
                    }
                )
            result = dict(changed=True, output="ThingType {} has been created".format(name), name=name)
        else:
            result = dict(changed=False, output="ThingType does not exist", name=name)

    module.exit_json(**result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
