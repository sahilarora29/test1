# Changelog

#### 1.1.0 - January 17, 2017
- Increase default wait timeout to 15 minutes
- Upgrade dependency to 1.0.0 hsingh.elasticbeanstalk

#### 1.0.0 - December 9, 2016
- Output CNAME record after creation

#### 1.0.0-rc2 - August 28, 2015
- Add check for empty Route53 zone
- Add support for different artifact extensions

#### 1.0.0-rc1 - July 20, 2015
- Initial release
