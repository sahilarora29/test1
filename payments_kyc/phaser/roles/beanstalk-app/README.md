Beanstalk App
=========

This role helps with creating AWS Elastic Beanstalk Application, Version and Environment

Requirements
------------

This role relies on `hsingh.elastic_beanstalk` role.

Role Variables
--------------

* `region`: AWS region in which to create the Beanstalk application, e.g. us-west-1
* `appName`: Name of the Beanstalk application, e.g. horizon-sfo-server
* `envName`: Name of the Beanstalk environment, e.g. horizon-sfo-dev-server
* `serviceName`: Name of the service being created, e.g. foundation, server
* `artifactVersion`: Name of the version being deployed, e.g. 1.0.123
* `artifactFilename`: Name of file downloaded from nexus.
* `nexusUrl`: Location of the nexus artifact server to use, defaults to http://nexus.pbi.global.pvt:8080/nexus
* `nexusRepository`: Nexus repository to download the artifact from, defaults to PBNexusRelease
* `nexusArtifactId`: Id of the artifact to download from nexus. Artifact id is a string of the form `<groupId>:<artifactId>:<artifactVersion>:<classifier>`, e.g. `com.pb.saas.embark:embark-server:1.0.2.8d98b56:beanstalk`. Classifier is optional.
* `versionsBucketName`: Name of the S3 bucket into which the version should be uploaded
* `optionSettings`: Array of elastic beanstalk options
* `doVersionChange`: if `true` version change is applied to environment
* `doOptionsChange`: if `true` configuration changes are applied to environment
* `r53zone`: Name of the Route 53 zone in which to create the CNAME record, optional, e.g. hsingh.pitneycloud.com
* `envUrl`: CNAME record to create for the environment, e.g. ui-qa-iad.hsingh.pitneycloud.com

This role registers a variable called `beanstalk` with the result of the environment creation


Example Playbook
----------------

Here is an example of how to create a beanstalk environment:

    - hosts: localhost
      connection: local
      gather_facts: False
      vars:
        region: us-west-1
        appName: embark-sfo-server
        envName: embark-sfo-dev-server
        serviceName: server
        artifactVersion: "1.0.2.8d98b56"
        artifactFilename: embark-server-{{ artifactVersion }}-beanstalk.zip
        nexusArtifactId: com.pb.saas.embark:embark-server:{{ artifactVersion }}:beanstalk
        versionsBucketName: "{{ s3.appVersions }}"
        r53zone: hsingh.pitneycloud.com
        envUrl: server-dev-sfo.hsingh.pitneycloud.com
      roles:
        - beanstalk_app


Author Information
------------------

[Harpreet Singh](mailto:harpreet.singh@pb.com)
