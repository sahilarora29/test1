# Changelog

#### 1.5.1 - *Unreleased*
- Remove ec2.py since we don't really need to connect to ec2 instances in phaser

#### 1.5.0 - Jan. 17th 2017
- Add support for TCP passthrough ELB
- Add limited support for lambda based service
- Add root volume size option
- Option to disable HTTP access
- Configurable AppDynamics app name
- Add support for RDS parameter groups
- Add support for sandbox, pre-prod and performance environments
- Add support for Ansible 2.x
- Add support for command timeout, notification end point & log publication
- `serviceCompute.beanstalk.minInstancesInService` parameter added
- Add ability to run custom pre/post tasks
- Fix: MaxSize was not set for beanstalk autoscaling group
- Fix: Use absolute path for `deploy.yml`

#### 1.1.0 - Nov. 9th 2015
- Add support for single instance type beanstalk environment
- Always download service dependencies
- Services can now have vanity or "simple" URLs in addition to the one assigned by phaser
- Add support for running Ansible tasks after compute resources are created.
- Fix: Beanstalk healthcheck URL is set if `serviceCompute.beanstalk.healthURL` is specified.

#### 1.0.0 - Sep. 18th 2015
- Initial release
