#!/bin/sh

WARN_LEVEL_STORAGE_THRESHOLD="${WARN_LEVEL_STORAGE_THRESHOLD:=3145728}" # 3GB for warning
CRITICAL_LEVEL_STORAGE_THRESHOLD="${CRITICAL_LEVEL_STORAGE_THRESHOLD:=2097152}" # 2GB for critical
FATAL_LEVEL_STORAGE_THRESHOLD="${FATAL_LEVEL_STORAGE_THRESHOLD:=1048576}" # 1GB for fatal

WARN_LEVEL_INODE_THRESHOLD="${WARN_LEVEL_INODE_THRESHOLD:=50000}" # about 10% of total
CRITICAL_LEVEL_INODE_THRESHOLD="${CRITICAL_LEVEL_INODE_THRESHOLD:=25000}" # about 5% of total
FATAL_LEVEL_INODE_THRESHOLD="${FATAL_LEVEL_INODE_THRESHOLD:=1000}" # if it gets this low, there is something definitely wrong

# find available storage and inodes
AVAILABLE_STORAGE="$(df / -k --output=avail | tail -1)"
AVAILABLE_INODES=$(df / --output=iavail | tail -1)

# Get instance id and region
INSTANCE_ID=$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)
REGION=$(wget -q -O - http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

TERMINATE=0

if [ $AVAILABLE_STORAGE -lt $FATAL_LEVEL_STORAGE_THRESHOLD -o $AVAILABLE_INODES -lt $FATAL_LEVEL_INODE_THRESHOLD ]; then
  TERMINATE=1
  echo "$(date +%m/%d/%Y' '%H:%M:%S) FATAL $INSTANCE_ID $REGION Disk health is at fatal level, Terminating the instance. Available storage: $AVAILABLE_STORAGE and Available inodes: $AVAILABLE_INODES" >> /var/log/disk.log
elif [ $AVAILABLE_STORAGE -lt $CRITICAL_LEVEL_STORAGE_THRESHOLD -o $AVAILABLE_INODES -lt $CRITICAL_LEVEL_INODE_THRESHOLD ]; then
  echo "$(date +%m/%d/%Y' '%H:%M:%S) CRITICAL $INSTANCE_ID $REGION Disk health is at critical level. Available storage: $AVAILABLE_STORAGE and Available inodes: $AVAILABLE_INODES" >> /var/log/disk.log
elif [ $AVAILABLE_STORAGE -lt $WARN_LEVEL_STORAGE_THRESHOLD -o $AVAILABLE_INODES < $WARN_LEVEL_INODE_THRESHOLD ]; then
  echo "$(date +%m/%d/%Y' '%H:%M:%S) WARN $INSTANCE_ID $REGION Disk health degraded, Available storage: $AVAILABLE_STORAGE and Available inodes: $AVAILABLE_INODES" >> /var/log/disk.log
else
  echo "$(date +%m/%d/%Y' '%H:%M:%S) INFO $INSTANCE_ID $REGION Disk healthy, Available storage: $AVAILABLE_STORAGE and Available inodes:  $AVAILABLE_INODES" >> /var/log/disk.log
fi

if [ $TERMINATE -eq 1 ]; then
  # find elb
  ELB_NAME=$(aws elb --region $REGION describe-load-balancers | jq -r '.LoadBalancerDescriptions[] | select(.Instances[].InstanceId == $INSTANCE_ID) | .LoadBalancerName' --arg INSTANCE_ID $INSTANCE_ID)

  # find dns name
  DNS_NAME=$(aws elb --region $REGION describe-load-balancers | jq -r '.LoadBalancerDescriptions[] | select(.Instances[].InstanceId == $INSTANCE_ID) | .DNSName' --arg INSTANCE_ID $INSTANCE_ID)

  # find application name
  APPLICATION_NAME=$(aws elasticbeanstalk describe-environments --region $REGION | jq -r '.Environments | .[] | select(.EndpointURL == $DNS_NAME) | .ApplicationName' --arg DNS_NAME $DNS_NAME)

  # find environment name
  ENVIRONMENT_NAME=$(aws elasticbeanstalk describe-environments --region $REGION | jq -r '.Environments | .[] | select(.EndpointURL == $DNS_NAME) | .EnvironmentName' --arg DNS_NAME $DNS_NAME)

  # find minumum configured instances
  MINIMUM_INSTANCES_IN_SERVICE=$(aws elasticbeanstalk describe-configuration-settings --environment-name $ENVIRONMENT_NAME --application-name $APPLICATION_NAME --region $REGION | jq -r '.ConfigurationSettings | .[].OptionSettings | .[] | select(.OptionName == "MinSize") | .Value')

  # find number of instance in service
  INSTANCES_IN_SERVICE=$(aws elb describe-instance-health --load-balancer-name --region $REGION $ELB_NAME | jq -r 'select(.InstanceStates[].State == "InService") | .[] |  length' --arg ELB_NAME $ELB_NAME)

  if [ ! $MINIMUM_INSTANCES_IN_SERVICE -gt $INSTANCES_IN_SERVICE ]; then
    halt -P;
  else
    echo "$(date +%m/%d/%Y' '%H:%M:%S) ERROR $INSTANCE_ID $REGION number of instances in service: $INSTANCES_IN_SERVICE is less than minimum number of configured instances: $MINIMUM_INSTANCES_IN_SERVICE, waiting to terminate..." >> /var/log/disk.log
  fi
fi
