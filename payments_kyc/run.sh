#!/bin/sh
set -e

JAVA_OPTS=""

if [ "$APPD_APP_NAME" != "" ]; then
    JAVA_OPTS="-javaagent:/opt/appdynamics/app-agent/javaagent.jar -Dappdynamics.agent.applicationName=${APPD_APP_NAME} -Dappdynamics.agent.tierName=${APPD_TIER_NAME} -Dappdynamics.agent.reuse.nodeName=${APPD_REUSE_NODENAME} -Dappdynamics.agent.reuse.nodeName.prefix=${APPD_NODENAME_PREFIX} -Dappdynamics.agent.accountName=${APPD_ACCT_NAME} -Dappdynamics.controller.hostName=${APPD_CONT_HOST} -Dappdynamics.controller.port=${APPD_CONT_PORT} -Dappdynamics.controller.ssl.enabled=${APPD_CONT_SSL_ENABLED} -Dappdynamics.agent.accountAccessKey=${APPD_ACCESS_KEY} -Dappdynamics.agent.uniqueHostId=$(curl -sS --retry 5 --retry-max-time 30 http://169.254.169.254/latest/meta-data/instance-id)"
fi

JAVA_OPTS="-Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/urandom $JAVA_OPTS"

exec java $JVM_OPTS $JAVA_OPTS -jar /opt/app/server.jar
