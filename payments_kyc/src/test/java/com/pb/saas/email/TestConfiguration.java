package com.pb.saas.email;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="mailto:harpreet.singh@pb.com">Harpreet Singh</a>
 */
@SpringBootApplication
public class TestConfiguration {

}

