package com.pb.saas.template;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:harpreet.singh@pb.com">Harpreet Singh</a>
 */
@Slf4j
@RestController
@RequestMapping("/")
@Api(description = "Welcome", value = "welcome")
public class WelcomeController {

    private final String versionNumber;

    @Autowired
    public WelcomeController(@Value("${info.version:Unknown}") String versionNumber) {
        this.versionNumber = versionNumber;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> create(HttpServletRequest request) {
        Map<String, String> response = new HashMap<>(2);
        response.put("name", "PROJECT_NAME");
        response.put("version", versionNumber);
        response.put("docs", request.getRequestURL().append("swagger-ui.html").toString());

        log.info("Handling welcome request with version: {}", versionNumber);

        return ResponseEntity.ok(response);
    }
}




